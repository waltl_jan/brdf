//Holds the scene's parameters 
//Represents CL version of Renderer::Scene_cl c++ struct. Packed should hopefully ensure the correct size.
typedef struct __attribute__ ((packed)) Scene_tag
{
	//float4 because c++ code does not support float3. Fourth component is unused for cam***.
	float4 camPos;
	float4 camDir;//normalized
	float4 camUp;//normalized
	int numSpheres;
	int numPointLights;
} Scene;

//Represents a point light
//Represents CL version of PointLight::PointLight_cl c++ struct. Packed should hopefully ensure the correct size.
typedef struct __attribute__ ((packed)) PointLight_tag
{
	//float4 because c++ code does not support float3. Fourth component is unused for both pos & color.
	float4 pos;
	float4 color;
	float intensity;
} PointLight;

typedef struct Ray_t
{
	float3 o,dir;
	float tMax;
} Ray;

typedef struct HitInfo_t
{
	float3 worldNormal;
	//int matID;
} HitInfo;

//Returns smaller root and true or false if eq does not have roots
bool solveQuad(float a, float b, float c, float* x0)
{
	float D = b*b-4.0f*a*c;
	if(D<0) return false;
	else if(D==0)
		*x0=-0.5f*b/a;
	else
		*x0 = 0.5f* (-b - sqrt(D))/a; 
		//TODO return both roots
	return true;
}
//Tests passed ray against the sphere. 
//Returns whether the ray hit the sphere and in that case updates HitInfo and sets r->tMax to that hit-point.
bool raySphereTest(float4 sphere, Ray* r,HitInfo* hit)
{
	if(r->tMax<=0.0f)
		return false;

	//solves ||x-sphere.xyz||^2 = r^2 ; x=r->o + t*r->dir;  for t
	float3 sR = r->o - sphere.xyz;//Vector from sphere to ray's origin
	
	//t*t*dot(r->dir,r->dir) + 2*t*dot(r->dir,sR) + dot(sR,sR)-r^2=0
	float a = 1.0f;//r->dir is normalized
	float b = 2.0f*dot(r->dir,sR);
	float c = dot(sR,sR) - sphere.w*sphere.w;
	float t;
	//TODO fix case where ray starts inside a sphere
	//No hit or too far
	if(!solveQuad(a,b,c,&t) || t>r->tMax || t<=0.0f)return false;	
	//We have a hit
	r->tMax=t;
	//Vector from sphere to the hitPoint on its surface. Its length should be radius => normalize;
	hit->worldNormal = (r->o + t*r->dir -sphere.xyz)/sphere.w; 
	return true;
}

//Traces the ray against the scene.
//Returns whether the ray hit anything and in that case updates HitInfo and sets r->tMax to that hit-point.
bool traceRay(__global float4* spheres,  Scene* scene, Ray* r,HitInfo* hit)
{
	bool hitTest=false;
	for(int i=0;i<scene->numSpheres;++i)
		hitTest |= raySphereTest(spheres[i],r,hit);
	//Plane
	float3 pN = (float3)(0.0f,1.0f,0.0f);
	float3 p0 = (float3)(0.0f,-1.3f,0.0f);
	float d = dot(r->dir,pN);
	if(d<=0)
	{
		float t = dot((p0 - r->o),pN)/d;
		if(t>=0 && t<=r->tMax)
		{
			hitTest=true;
			hit->worldNormal=pN;
			r->tMax=t;
		}
	}
	return hitTest;
}

bool traceShadowRay(__global float4* spheres, Scene* scene,float3 from, float3 to)
{
	Ray r;
	r.o = from;
	r.dir = to-from;
	float dist = sqrt(dot(r.dir,r.dir));
	r.o+=0.001f*r.dir;//RESOLVE Epsilon
	r.dir/=dist;
	
	r.tMax=dist;
	HitInfo dump;
	return traceRay(spheres,scene,&r,&dump);
}

//Generates ray from the camera based on threadID.
//TODO Also base it on rng to allow multiple samples per pixel
Ray genCamRay(Scene* scene)
{
	float2 rOffset = (float2)(get_global_id(0)/ (float)( get_global_size(0)),
						 get_global_id(1)/ (float)( get_global_size(1)));
	rOffset = rOffset*2.0f - (float2)(1.0f);
	rOffset.y*=-1.0f;//y is up
	
	float3 right = cross(scene->camDir.xyz,scene->camUp.xyz);
	
	Ray r;
	r.o =scene->camPos.xyz;
	r.dir = scene->camDir.xyz + rOffset.x*right + rOffset.y*scene->camUp.xyz;
	r.dir = normalize(r.dir);
	r.tMax = FLT_MAX;
	return r;
}
float3 BRDFWhiteLambert(float3 wIn, float3 wOut, float3 n)
{
	//White object
	return (float3)(1.0f) / 3.14f;
}
float3 BRDFBlinnPhong(float3 wIn, float3 wOut, float3 n)
{
	//Those would be the parameters of this BRDF
	float3 diffColor = (float3)(1.0f);//Colors of the surface
	float3 specColor = (float3)(1.0f);//Should be the light's color, that means white in BRDF evaluation
	float roughness = 64.0f;
	
	float3 diffuse = BRDFWhiteLambert(wIn, wOut, n);
	float3 halfVec = normalize(wIn+wOut);
	float3 spec = pow(dot(halfVec,n),roughness);
	return diffuse+spec;
}

//In world coordinates for now
//both vector point away from the surface and all three are outside
float3 BRDF(float3 wIn, float3 wOut, float3 normal)
{
	//return BRDFBlinnPhong(wIn,wOut,normal);
	return BRDFWhiteLambert(wIn,wOut,normal);
}

//From http://cas.ee.ic.ac.uk/people/dt10/research/rngs-gpu-mwc64x.html
float MWC64X(uint2 *state)
{
    enum { A=4294883355U};
    uint x=(*state).x, c=(*state).y;  // Unpack the state
    uint res=x^c;                     // Calculate the result
    uint hi=mul_hi(x,A);              // Step the RNG
    x=x*A+c;
    c=hi+(x<c);
    *state=(uint2)(x,c);               // Pack the state back up
    return res/(float)4294967295;      // Return the next result
}

__kernel void render(__global float4* spheres,__global PointLight* pLights, Scene scene,int numSamples, __global float4 * buffImage, __global float4* outImage, __global uint2* rngState)
{
	int tID= get_global_id(0) + get_global_size(0)*get_global_id(1);
	uint2 tRngState = rngState[tID];

	Ray ray = genCamRay(&scene);
	HitInfo hit;
	bool hitted =false;
	float3 color=(float3)(0.0f);
	float3 throughput=(float3)(1.0f);
	for(int n=0;n<5;++n)
	{
		hitted=traceRay(spheres,&scene,&ray,&hit);
		if(hitted)//We've hit an object
		{
			float3 hitPoint = ray.o + ray.tMax * ray.dir;
			float3 wIn = normalize(scene.camPos.xyz - hitPoint);
			float3 n = hit.worldNormal;
			
			//Add light from PointLigts
			for(int i=0;i<scene.numPointLights;++i)
			{
				float3 lightPos = pLights[i].pos.xyz;
				float3 lightColor = pLights[i].color.xyz;
				float intensity = pLights[i].intensity;
				
				//Trace shadow ray
				if(traceShadowRay(spheres,&scene,hitPoint,lightPos))
					continue;
				
				float dist2 = dot(lightPos-hitPoint,lightPos-hitPoint);
				float3 Li=lightColor*intensity / (dist2+0.001f);			
				float3 wOut= normalize(lightPos - hitPoint);	
				
				float cosTheta = dot(wOut,n);
				if(cosTheta>=0)//Is a reflection
					color+=Li *throughput* BRDF( wIn, wOut,n)*cosTheta;//Transfer photons from light to camera
			}	
			//Tangent space
			float3 tn1=cross(n,wIn);
			float3 tn2=cross(tn1,n);
			//generate random number
			float rng1=MWC64X(&tRngState);
			float rng2=MWC64X(&tRngState);
			
			float3 newDir = normalize(n + tn1*rng1 + tn2*rng2);//Is this cosine weighted???
			Ray newRay;
			newRay.dir=newDir;
			newRay.o=hitPoint+newDir*0.001f;//RESOLVE Epsilon
			throughput*= BRDF( wIn,newDir ,n);//*dot(newDir,n);
			ray=newRay;//Bounce
			
		}
		else
			break;
	}
	rngState[tID] = tRngState;
	float nS=numSamples;
	buffImage[tID]+=(float4)(color,1.0f);
	float4 outputCol = buffImage[tID]/nS;
	outImage[tID] = outputCol;		
}