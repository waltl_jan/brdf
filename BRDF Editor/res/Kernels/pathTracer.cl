#define PI 3.1415926535f
#define TWOPI (2.0f*PI)
#define HALFPI (PI/2.0f)
#define EPSILON 0.000001f
//Holds the scene's parameters 
//Represents CL version of Renderer::Scene_cl c++ struct. Packed should hopefully ensure the correct size.
typedef struct __attribute__ ((packed)) Scene_tag
{
	//float4 because c++ code does not support float3. Fourth component is unused for cam***.
	float4 camPos;
	float4 camDir;//normalized
	float4 camUp;//normalized
	int numSpheres;
	int numMeshes;
	int numPointLights;
	int numAreaLights;
} Scene;

//Represents a point light
//Represents CL version of PointLight::PointLight_cl c++ struct. Packed should hopefully ensure the correct size.
typedef struct __attribute__ ((packed)) PointLight_tag
{
	//float4 because c++ code does not support float3. Fourth component is unused for both pos & color.
	float4 pos;
	float4 color;
	float intensity;
} PointLight;

//Represents an area light
//Represents CL version of AreaLight::AreaLight_cl c++ struct. Packed should hopefully ensure the correct size.
typedef struct __attribute__ ((packed)) AreaLight_tag
{
	//float4 because c++ code does not support float3. Fourth component is unused for pos, color & normal.
	float4 pos;
	float4 normal;
	float4 color;
	float intensity;
} AreaLight;

typedef struct __attribute__ ((packed)) Param_t 
{
	union __attribute__ ((packed))
	{
		int i;
		float f1;
		float2 f2;
		float3 f3;
		float4 f4;
	}value;
}Param;

typedef struct __attribute__ ((aligned (64))) BVHNode_t
{
	int children[2];
	float childBounds[12];
	char padding[8];
} BVHNode;

typedef struct __attribute__ ((aligned (64))) Triangle_t
{
	float4 v0,e1,e2,n;
} Triangle;

typedef struct __attribute__ ((packed)) Mesh_t
{
	int begin,end,rootIndexNode;
} Mesh;

typedef struct __attribute__ ((packed)) MatInfo_t
{
	int brdfID;
	int paramOff;
} MatInfo;

typedef struct HitInfo_t
{
	float3 hitPoint;
	float3 worldNormal;
	MatInfo matInfo;
} HitInfo;

typedef struct Ray_t
{
	float3 o,dir;
	float tMax;
} Ray;

typedef uint2 RNGState;

//Declarations will be inserted here.
//It allows them to use i.e RNGState type.
//DECLHERE
float3 genCosineDir(float3 wIn, float3 n, RNGState* rngState, float* pdf);
bool traceShadowRay(global float4* spheres,global Mesh* meshes, global Triangle* triangles, global BVHNode* bvhNodes, global MatInfo* matInfo, Scene* scene,float3 from, float3 to);
void genBasis(float3 n, float3* out1,float3* out2);


//From http://cas.ee.ic.ac.uk/people/dt10/research/rngs-gpu-mwc64x.html
float MWC64X(RNGState *state)
{
    enum { A=4294883355U};
    uint x=(*state).x, c=(*state).y;  // Unpack the state
    uint res=x^c;                     // Calculate the result
    uint hi=mul_hi(x,A);              // Step the RNG
    x=x*A+c;
    c=hi+(x<c);
    *state=(uint2)(x,c);               // Pack the state back up
    return res/(float)4294967295;      // Return the next result
}

float uniformRNG(RNGState* state)
{
	return MWC64X(state);
}

//Returns smaller root and true or false if eq does not have roots
bool solveQuad(float a, float b, float c, float* x0)
{
	float D = b*b-4.0f*a*c;
	if(D<0) return false;
	else if(D==0)
		*x0=-0.5f*b/a;
	else
		*x0 = 0.5f* (-b - sqrt(D))/a; 
		//TODO return both roots
	return true;
}
//Tests passed ray against the sphere. 
//Returns whether the ray hit the sphere and in that case updates HitInfo and sets r->tMax to that hit-point.
bool raySphereTest(float4 sphere,MatInfo matInfo, Ray* r,HitInfo* hit)
{
	if(r->tMax<=0.0f)
		return false;
	//solves ||x-sphere.xyz||^2 = r^2 ; x=r->o + t*r->dir;  for t
	
	float3 sR = r->o - sphere.xyz;//Vector from sphere to ray's origin
	
	//t*t*dot(r->dir,r->dir) + 2*t*dot(r->dir,sR) + dot(sR,sR)-r^2=0
	float a = 1.0f;//r->dir is normalized
	float b = 2.0f*dot(r->dir,sR);
	float c = dot(sR,sR) - sphere.w*sphere.w;
	float t;
	//TODO fix case where ray starts inside a sphere
	//No hit or too far
	if(!solveQuad(a,b,c,&t) || t>r->tMax || t<=0.0f)
		return false;	
	//We have a hit
	r->tMax=t;

	hit->hitPoint = r->o+t*r->dir;
	//r->o + t*r->dir -sphere.xyz = vector from sphere's center to the hitPoint
	hit->worldNormal = (sR + t*r->dir);
	hit->worldNormal = normalize(hit->worldNormal);
	hit->matInfo = matInfo;
	return true;
}

bool rayPlaneTest(float3 planeN,float3 plane0, Ray* r,HitInfo* hit)
{
	float d = dot(r->dir,planeN);
	if(d<=0)
	{
		float t = dot((plane0 - r->o),planeN)/d;
		if(t>=0 && t<=r->tMax)
		{
			hit->worldNormal=planeN;
			hit->matInfo.brdfID=1;
			hit->matInfo.paramOff=0;
			hit->hitPoint = r->o+t*r->dir;
			r->tMax=t;
			return true;
		}
	}
	return false;
}

bool rayDiscTest(float3 discN, float3 discCenter, float radius, Ray* r, HitInfo* hit)
{
	float d = dot(r->dir,discN);
	if(d<=0)
	{
		float t = dot((discCenter - r->o),discN)/d;
		float3 hitPoint = r->o + t*r->dir;
		float dist2 = dot(discCenter -hitPoint,discCenter-hitPoint);
		if(t>=0 && t<=r->tMax && dist2<radius*radius )
		{
			r->tMax=t;
			hit->worldNormal=discN;
			hit->matInfo.brdfID=1;
			hit->matInfo.paramOff=0;
			hit->hitPoint = r->o+t*r->dir;
			return true;
		}
	}
	return false;
}


bool rayTriangleTest(Triangle tri, MatInfo matInfo,Ray* r, HitInfo* hit)
{
	float3 v0=tri.v0.xyz;
	float3 e1=tri.e1.xyz;
	float3 e2=tri.e2.xyz;

	if(fabs(dot(tri.n.xyz,r->dir))<EPSILON)
		return false;
	float3 pvec = cross(r->dir,e2);
	float det = dot(e1,pvec);
	if(det<EPSILON && det>-EPSILON)
		return false;
	float f= 1.0f/det;
	float3 dist=r->o-v0;
	float u =dot(pvec,dist)*f;
	if(	u<0.0f || u>1.0f)
		return false;
	float3 qvec = cross(dist,e1);
	float v = dot(r->dir,qvec) * f;
	if(v<0.0f || u+v>1.0f)
		return false;
	float t = f* dot(e2,qvec);
	if(t>0.0f && t < r->tMax)
	{
		r->tMax=t;
		hit->worldNormal=tri.n.xyz;
		hit->matInfo=matInfo;
		hit->hitPoint=r->o+t*r->dir;
		return true;
	}
	return false;
}

bool rayTriangleTest2(Triangle tri, MatInfo matInfo,Ray* r, HitInfo* hit)
{
	float3 v0=tri.v0.xyz;
	float3 e1=tri.e1.xyz;
	float3 e2=tri.e2.xyz;
	float3 v1=v0+e1;
	float3 v2=v0+e2;
	float3 n=tri.n.xyz;

	// compute plane's normal
    float3 v0v1 = v1 - v0; 
    float3 v0v2 = v2 - v0; 
    // no need to normalize
    float3 N = cross(v0v1,v0v2); // N 
 
    // Step 1: finding P
 
    // check if ray and plane are parallel ?
    float NdotRayDirection = dot(N,r->dir); 
    if (fabs(NdotRayDirection) < EPSILON) // almost 0 
        return false; // they are parallel so they don't intersect ! 
 
    // compute d parameter using equation 2
    float d = dot(N,v0); 
 
    // compute t (equation 3)
    float t = (dot(N,r->o) + d) / NdotRayDirection; 
    // check if the triangle is in behind the ray
    if (t < 0 || t>r->tMax) return false; // the triangle is behind 
 
    // compute the intersection point using equation 1
    float3 P = r->o + t * r->dir; 
 
    // Step 2: inside-outside test
    float3 C; // vector perpendicular to triangle's plane 
 
    // edge 0
    float3 edge0 = v1 - v0; 
    float3 vp0 = P - v0; 
    C = cross(edge0,vp0); 
    if (dot(N,C) < 0) return false; // P is on the right side 
 
    // edge 1
    float3 edge1 = v2 - v1; 
    float3 vp1 = P - v1; 
    C = cross(edge1,vp1); 
    if (dot(N,C) < 0)  return false; // P is on the right side 
 
    // edge 2
    float3 edge2 = v0 - v2; 
    float3 vp2 = P - v2; 
    C = cross(edge2,vp2); 
    if (dot(N,C) < 0) return false; // P is on the right side; 
 
    r->tMax=t;
	hit->worldNormal=N;
	hit->matInfo=matInfo;
	hit->hitPoint=P;
	return true;
}
bool rayAABBTest(float3 AABBmin,float3 AABBmax,Ray* r)
{
	//https://tavianator.com/fast-branchless-raybounding-box-intersections/
	float tmin = -FLT_MAX, tmax = FLT_MAX;
	float t1,t2;

	if (r->dir.x > EPSILON || r->dir.x < - EPSILON) {
        t1 = (AABBmin.x - r->o.x)/r->dir.x;
        t2 = (AABBmax.x - r->o.x)/r->dir.x;
 
        tmin = fmax(tmin, fmin(t1, t2));
        tmax = fmin(tmax, fmax(t1, t2));
    }

	if (r->dir.y > EPSILON || r->dir.y < - EPSILON) {
        t1 = (AABBmin.y - r->o.y)/r->dir.y;
        t2 = (AABBmax.y - r->o.y)/r->dir.y;
 
        tmin = fmax(tmin, fmin(t1, t2));
        tmax = fmin(tmax, fmax(t1, t2));
    }

	if (r->dir.z > EPSILON || r->dir.z < - EPSILON) {
        t1 = (AABBmin.z - r->o.z)/r->dir.z;
        t2 = (AABBmax.z - r->o.z)/r->dir.z;
 
        tmin = fmax(tmin, fmin(t1, t2));
        tmax = fmin(tmax, fmax(t1, t2));
    }
	
	return tmin<=tmax && tmin<r->tMax && tmin>=0.0f;
}
bool rayBVHTest(int rootNode, global BVHNode* nodes,global Triangle* triangles, MatInfo matInfo,Ray* r,HitInfo* hit)
{
	bool hitTest=false;
	#define STACK_SIZE 64
	int stack[STACK_SIZE];
	int stackTop=0;
	stack[0]=rootNode;
	while(stackTop>=0)
	{
		BVHNode node=nodes[stack[stackTop--]];
		
		if(node.children[0]>=0)//Internal node
		{
			if(rayAABBTest(vload3(0,&node.childBounds[0]),vload3(1,&node.childBounds[0]),r))
				stack[++stackTop]=node.children[0];
			if(rayAABBTest(vload3(2,&node.childBounds[0]),vload3(3,&node.childBounds[0]),r))
				stack[++stackTop]=node.children[1];
		}
		else
		{
			for(int i=0;i<-node.children[0];++i)
			{
				int tIndex = node.children[1]+i;
				hitTest |=rayTriangleTest(triangles[tIndex],matInfo,r,hit);
			}
		}
	}
	
	return hitTest;
}
//Traces the ray against the scene.
//Returns whether the ray hit anything and in that case updates HitInfo and sets r->tMax to that hit-point.
bool traceRay(global float4* spheres,
			  global Mesh* meshes,
			  global Triangle* triangles,
			  global BVHNode* bvhNodes,
			  global MatInfo* matInfos,
			  Scene* scene,
			  Ray* r,
			  HitInfo* hit)
{
	bool hitTest=false;

	for(int i=0;i<scene->numSpheres;++i)
		hitTest |= raySphereTest(spheres[i],matInfos[i],r,hit);
	
	for(int i=0;i<scene->numMeshes;++i)
	{
		Mesh mesh=meshes[0];
		MatInfo mat= matInfos[scene->numSpheres];//TODO fix
	
		//for(int tIndex=mesh.begin;tIndex<mesh.end;++tIndex)
		//{
		//	Triangle t = triangles[tIndex];
		//	hitTest|=rayTriangleTest(t,mat,r,hit);
		//}
		hitTest|=rayBVHTest(mesh.rootIndexNode,bvhNodes,triangles,mat,r,hit);
	}
	
	//Plane
	if(rayPlaneTest((float3)(0.0f,1.0f,0.0f),(float3)(0.0f,-0.5f,0.0f),r,hit))
	{//TEMP set correct material
		hitTest=true;
		hit->matInfo = matInfos[0];
	}
	
	return hitTest;
}

bool traceShadowRay(global float4* spheres,
				    global Mesh* meshes,
					global Triangle* triangles,
					global BVHNode* bvhNodes,
					global MatInfo* matInfos,
					Scene* scene,
					float3 from,
					float3 to)
{
	//return false;
	Ray r;
	r.o = from;
	r.dir = to-from;
	float dist = sqrt(dot(r.dir,r.dir));
	r.o+=0.001f*r.dir;//RESOLVE Epsilon
	r.dir/=dist;
	
	r.tMax=dist-0.001f;
	HitInfo dump;
	return traceRay(spheres,meshes,triangles,bvhNodes,matInfos,scene,&r,&dump);
}

//Generates ray from the camera based on threadID.
Ray genCamRay(Scene* scene,RNGState* rngState)
{
	float2 pixelOffset = (float2)(uniformRNG(rngState),uniformRNG(rngState));
	float2 pixelSize = (float2)(1.0f/ get_global_size(0),1.0f/ get_global_size(1));
	float2 offset = (float2)(get_global_id(0),get_global_id(1)) + pixelOffset;
	//-1.0,+1.0 coordinates on the screen
	float2 rOffset = offset*pixelSize*2.0f - (float2)(1.0f);
	rOffset.y*=-1.0f;//y is up
	
	float3 right = cross(scene->camDir.xyz,scene->camUp.xyz);
	
	Ray r;
	r.o =scene->camPos.xyz;
	r.dir = scene->camDir.xyz + rOffset.x*right + rOffset.y*scene->camUp.xyz;
	r.dir = normalize(r.dir);
	r.tMax = FLT_MAX;
	return r;
}

//Returns orthonormal basis constructed from passed normal(MUST be normalize).
void genBasis(float3 n, float3* out1,float3* out2)
{
	*out1 = fabs(n.x)>fabs(n.z) ? (float3)(n.y,-n.x,0.0f) : (float3)(0.0f,n.z,-n.y);
	*out1 = normalize(*out1);
	*out2 = normalize(cross(n,*out1));
}
//Returns randomly generated direction in a hemisphere from cosine-weighted distribution.
float3 genCosineDir(float3 wIn, float3 n, RNGState* rngState, float* pdf)
{
	float e1=uniformRNG(rngState);
	float e2=uniformRNG(rngState);
	float angle = TWOPI*e1;
	float r = sqrt(e2);
	
	//Tangent space
	float3 tn1,tn2;
	genBasis(n,&tn1,&tn2);

	float3 offset = cos(angle)*tn1 + sin(angle)*tn2;
	float3 wOut= sqrt(1-e2)*n + r*offset;
	*pdf = dot(n,wOut)/PI;
	return wOut;
}

void dirSampleHDRMap(read_only image2d_t sky, RNGState* rng,float3* outDir, float3* outColor)
{
	int w = get_image_width(sky);
	int h = get_image_height(sky);
	float u = uniformRNG(rng);
	float v = uniformRNG(rng);
	float3 hOff= cos(u*TWOPI)*(float3)(1.0f,0.0f,0.0f) + sin(u*TWOPI)*(float3)(0.0f,0.0f,1.0f);	
	
	*outDir = cos(v*TWOPI-PI)*hOff + sin(v*TWOPI-PI)*(float3)(0.0f,1.0f,0.0f);
	*outColor = read_imagef(sky,(int2)(u*w,v*h)).xyz;
}
float3 sampleHDRMap(read_only image2d_t sky, float3 dir)
{
	int w = get_image_width(sky);
	int h = get_image_height(sky);
	//longtitude [-PI,PI] -> [0,1]
	float u = atan2(dir.z,dir.x)/(TWOPI) + 0.5f;
	//latitude [0,PI] -> [0,1]
	float v = acos(dir.y)/PI;
	return read_imagef(sky,(int2)(u*w,v*h)).xyz;
}
//Returns LI from given light
float3 sampleAreaLight(global AreaLight* light, float3 sampledLPos, float3 hitPos)
{	
	float3 lightNormal = light->normal.xyz;
	float3 lightColor = light->color.xyz;
	float3 sampledDir=normalize(hitPos-sampledLPos);
	float dist2 = dot(sampledLPos-hitPos,sampledLPos-hitPos);
	return light->intensity*lightColor*dot(sampledDir,lightNormal)/(dist2 + 0.001f);
}
float3 sampleAreaLightPos(global AreaLight* light,RNGState* rng)
{
	float3 lightNormal = light->normal.xyz;
	float3 lightPos = light->pos.xyz;
	float lightRadius = 1.0f;
	float3 out1,out2;
	genBasis(lightNormal,&out1,&out2);
	float e1 = lightRadius*uniformRNG(rng);
	float e2 = lightRadius*uniformRNG(rng);
	
	return lightPos + out1*e1 + out2*e2;
}
float3 sampleLights(global float4* spheres,
					global Mesh* meshes,
					global Triangle* triangles,
					 global BVHNode* bvhNodes,
					global MatInfo* matInfos,
					global Param* matParams,
					global PointLight* pLights,
					global AreaLight* aLights,
					Scene* scene,
					RNGState* rng,
					HitInfo* hitInfo,
					float3 wIn)
{
	float3 color=(float3)(0.0f);
	float3 hitPoint = hitInfo->hitPoint;
	float3 n = hitInfo->worldNormal;
	//Add light from PointLigts
	for(int i=0;i<scene->numPointLights;++i)
	{
		float3 lightPos = pLights[i].pos.xyz;
		float3 lightColor = pLights[i].color.xyz;
		float intensity = pLights[i].intensity;

		float3 wOut= normalize(lightPos - hitPoint);	
		float cosTheta = dot(wOut,n);
		//Trace shadow ray
		if(cosTheta< EPSILON || traceShadowRay(spheres,meshes,triangles,bvhNodes,matInfos,scene,hitPoint,lightPos))
			continue;

		float dist2 = dot(lightPos-hitPoint,lightPos-hitPoint);
		float3 Li=lightColor*intensity / (dist2+0.001f);			
		color+=Li*BRDFEval(hitInfo->matInfo,n,wIn,wOut,matParams)*cosTheta;
	}	

	for(int i=0;i<scene->numAreaLights;++i)
	{
		float3 lightPos = sampleAreaLightPos(aLights+i,rng);
		
		float3 wOut= normalize(lightPos - hitPoint);
		float cosTheta = dot(wOut,n);
		
		if(cosTheta<=EPSILON || traceShadowRay(spheres,meshes,triangles, bvhNodes,matInfos, scene, hitPoint, lightPos))
			continue;
		float3 Li=sampleAreaLight(aLights+i,lightPos,hitPoint);
		color+=Li*BRDFEval(hitInfo->matInfo,n,wIn,wOut,matParams)*cosTheta;
	}

	return color;
}
__kernel void render(global float4* spheres,
					 global Mesh* meshes,
					 global Triangle* triangles,
					 global BVHNode* bvhNodes,
					 global PointLight* pLights,
					 global AreaLight* aLights,
					 global MatInfo* matInfos,
					 global Param* matParams,
					 Scene scene,
					 int numSamples,
					 global float4 * buffImage,
					 write_only image2d_t outImage,
					 global uint2* rngState,
					 read_only image2d_t sky)
{
	int tID= get_global_id(0) + get_global_size(0)*get_global_id(1);
	RNGState tRngState = rngState[tID];

	Ray ray = genCamRay(&scene,&tRngState);
	HitInfo hit;
	bool hitted =false;
	float3 color=(float3)(0.0f);
	float3 throughput=(float3)(1.0f);
	for(int bounce=0;bounce<5;++bounce)
	{
		hitted=traceRay(spheres,meshes,triangles,bvhNodes, matInfos, &scene,&ray,&hit);
		//TODO other objects - offset their matIDs*
		if(hitted)//We've hit an object
		{
			float3 hitPoint = hit.hitPoint;
			float3 wIn = -ray.dir;
			float3 n = hit.worldNormal;
			//Add emmission light
			//Russian roulette to terminate the path
			float maxT = max(throughput.x,max(throughput.y,throughput.z));
			if(uniformRNG(&tRngState)>maxT)
				break;
			else
				throughput/=maxT;

			//Sample the lights
			color+=throughput*sampleLights(spheres,meshes,triangles,bvhNodes,matInfos,matParams,pLights,aLights,&scene,&tRngState,&hit,wIn);

			{//Sample HDR map
				float3 sampledDir;
				float3 sampledColor;
				dirSampleHDRMap(sky,&tRngState,&sampledDir,&sampledColor);
				float cosTheta = dot(sampledDir,n);
				//if(cosTheta>0)//Is a reflection
				//	color+=throughput*sampledColor*BRDFEval(hit.matID,n,wIn,sampledDir)*cosTheta*1.0f;
			}
			float3 wOut;
			float pdf;
			float3 val = BRDFSampleEval(hit.matInfo,n,wIn,&wOut,&tRngState,&pdf,matParams);
			float cosTheta = dot(wOut,n);
			
			if(cosTheta==0.0f || pdf==0.0f)//It does happen
				break;
				
			throughput*= val*cosTheta/pdf;
			ray.dir=wOut;
			ray.o=hitPoint+wOut*0.001f;//RESOLVE Epsilon
			ray.tMax=FLT_MAX;
		}
		else
		{
			//color+=throughput*1.0f* sampleHDRMap(sky,ray.dir);
			break;
		}
			
	}
	
	rngState[tID] = tRngState;
	float4 newCol = (float4)(color,1.0f);
	if(numSamples>0)//Add previous 
		newCol+=buffImage[tID];
	buffImage[tID]= newCol;	
	float4 imgCol = newCol/(float)(numSamples+1);
	float gamma = 2.2f;
	float exposure = 1.0f;
	float4 toneCol = pow(1.0f - exp(-imgCol*exposure),1.0f/2.2f);
	toneCol.w=1.0f;
	write_imagef(outImage,(int2)(get_global_id(0),get_global_id(1)),toneCol);
}