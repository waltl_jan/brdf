float3 BRDF(float3 n, float3 wIn, float3 wOut)
{
	return (float3)0.0f;
}
float3 SampleBRDF(float3 wIn, float3 n, float* wOut, float* pdf, RNGState* rngState)
{
	*pdf=1.0f;
	return -wIn + 2*dot(wIn,n)*n;
}