params{
float m; [min=0.01f] [max=1.2f] [step=0.1f]
float3 diffColor; [color]
float3 ior; [min=1.0f] [max=2.4f] [step=0.2f]
}

float3 BRDF(float3 n, float3 wIn, float3 wOut)
{
	
	float m2 = m*m;
	float3 h = normalize(wIn+wOut);
	float nh = dot(n,h);
	float nIn = dot(n,wIn);
	float nOut = dot(n,wOut);
	float hIn = dot(h,wIn);
	float hOut = dot(h,wOut);
	float nh2 = nh*nh;
	float nh4 = nh2 * nh2;
	//Beckmann
	float D = 1.0f/(PI*m2*nh4) * exp((1.0f-1.0f/nh2)/m2);
	//Fresnel, Shlick
	float3 F0 = (1.0f-ior)/(1.0f+ior);
	F0=F0*F0;
	float3 F = F0 + (1.0f - F0) * pow(1.0f - hOut,5.0f);
	//Geometry term
	float G = min(1.0f, min(2.0f*nh*nIn/hIn, 2.0f*nh*nOut/hIn));
	
	return (1.0f-F)*diffColor/PI+ F*G*D/(4*nIn*nOut);
}
float3 SampleBRDF(float3 n, float3 wIn, float3* wOut, float* pdf, RNGState* rngState)
{
	n=normalize(n);
	float e2=uniformRNG(rngState);
	float e1=uniformRNG(rngState);
	float m2= m*m;

	float loge1 = log(e1);
	loge1 = isinf(loge1)? 0.0f : loge1;
	float tan2th = -m2*loge1;
	float costh2 = 1.0f / (1.0f+tan2th);
	float costh = sqrt(costh2);
	float sinth = sqrt(1.0f-costh2);
	float phi = TWOPI * e2;
	
	//Tangent space
	float3 tn1,tn2;
	genBasis(n,&tn1,&tn2);
	
	float3 tangentOff = cos(phi)*tn1 + sin(phi)*tn2;
	float3 h = normalize(sinth*tangentOff + costh*n);
	if(dot(h,wIn)<0.0f)
		h*=-1.0f;
	*wOut = normalize(-wIn + 2.0f*dot(h,wIn)*h);
	if(dot(*wOut,n)<=0.0f)
	{
		*wOut = normalize(*wOut -2.0f*dot(n,*wOut)*n);
		h=normalize(wIn+*wOut);
	}

	//Evaluate the bRDF
	//TODO Replace with BRDF call when allowed
	float nh = dot(n,h);
	float nIn = dot(n,wIn);
	float nOut = dot(n,*wOut);
	float hIn = dot(h,wIn);
	float hOut = dot(h,*wOut);
	float nh2 = nh*nh;
	float nh4 = nh2 * nh2;
	
	//Beckmann
	float D = 1.0f/(PI*m2*nh4) * exp((1.0f-1.0f/nh2)/m2);
	//if(!isfinite(D))
	//	printf("NAN2 %f %f\n",nh4,nh2);
	//Fresnel, Shlick
	float3 F0 = (1.0f-ior)/(1.0f+ior);
	F0=F0*F0;
	float3 F = F0 + (1.0f - F0) * pow(1.0f - hOut,5.0f);
	//Geometry term
	float G = min(1.0f, min(2.0f*nh*nIn/hIn, 2.0f*nh*nOut/hIn));
	
	*pdf = 1.0f/(PI*m2*nh2*nh2) * exp((1.0f-1.0f/nh2)/m2);
	*pdf = *pdf * fabs(nh);
	*pdf = *pdf / dot(wIn,h) / 4.0f;
	//if(!isfinite(*pdf))
	//	printf("NANX %n\n",dot(wIn,h));
	

	float3 color = (1.0f-F)*diffColor/PI+ F*G*D/(4*nIn*nOut);;
	
	return color;
}