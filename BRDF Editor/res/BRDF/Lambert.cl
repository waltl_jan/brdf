params{
float3 color;
float3 col2; [color] [min=0.1f] [max=1.2f] [step=0.1f]
}

float3 BRDF(float3 n, float3 wIn, float3 wOut)
{
	//White object
	
	return (float3)(col2) / PI;
}
float3 SampleBRDF(float3 n, float3 wIn, float3* wOut, float* pdf, RNGState* rngState)
{
	*wOut=genCosineDir(wIn,n,rngState,pdf);
	return (float3)(col2) / PI;
}