#include "Program.hpp"

#include <chrono>
#include <iostream>

#include <glad/glad.h>
#include <ImGui/imgui.h>
#include <src/OpenCLRenderer/Objects/TriangleMesh.hpp>
#include <src/OpenCLRenderer/Scene.hpp>

#include "WindowContext.hpp"

namespace brdfEditor
{
	namespace
	{
		constexpr const std::size_t w = 1280;
		constexpr const std::size_t h = 720;

		GLuint initTexture(std::size_t w, std::size_t h)
		{
			GLuint texture;
			glCreateTextures(GL_TEXTURE_2D, 1, &texture);
			glTextureStorage2D(texture, 1, GL_RGBA32F, w, h);
			glTextureParameteri(texture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			// RESOLVE Is this necessary?
			std::vector<float> pixels(w*h * 4, 1.0f);
			glTextureSubImage2D(texture, 0, 0, 0, w, h, GL_RGBA, GL_FLOAT, pixels.data());
			return texture;
		}

		clRenderer::TriangleMesh loadTestMesh(const clRenderer::MaterialsLibrary& matLib)
		{
			using namespace clRenderer;
			auto&& triangles = TriangleMesh::loadModel("models/bunny.obj");

			auto&& cookTorr = matLib.getMaterial("CookTorrance");
			return TriangleMesh{ Transform{glm::vec3{0.0f,0.0f,0.0f}},cookTorr, std::move(triangles) };
		}
		clRenderer::Scene buildTestScene(const clRenderer::MaterialsLibrary& matLib)
		{
			using namespace clRenderer;
			Scene scene;
			auto&& cookTorr = matLib.getMaterial("CookTorrance");
			auto&& lambert = matLib.getMaterial("Lambert");
			auto&& mirror = matLib.getMaterial("Mirror");
			auto& spheres = scene.getSpheres();
			for (std::size_t i = 0; i < 1; ++i)
			{
				spheres.emplace_back(Transform(glm::vec3(cos(3.14 / 5 * i), sin(3.14 / 5 * i), -0.5f*i)), cookTorr, 0.3f);
				spheres.emplace_back(Transform(glm::vec3(cos(3.14 / 5 * i + 3.14), sin(3.14 / 5 * i + 3.14), -0.5f*i)), lambert, 0.3f);
			}

			auto& meshes = scene.getMeshes();
			meshes.push_back(std::move(loadTestMesh(matLib)));

			float mult = 1.0f;
			auto& pointLights = scene.getPointLights();
			//pointLights.emplace_back(glm::vec3(0.0f, +0.0f, +2.0f), mult*6.0f, glm::vec3(1.0f, 1.0f, 1.0f));
			//pointLights.emplace_back(glm::vec3(-2.0f, +0.3f, -2.0f), mult*5.0f, glm::vec3(1.0f, 1.0f, 1.0f));
			//pointLights.emplace_back(glm::vec3(2.0f, +0.0f, -2.0f), mult*5.0f, glm::vec3(1.0f, 1.0f, 1.0f));
			//pointLights.emplace_back(glm::vec3(0.0f, +2.0f, -2.0f), mult*1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
			//pointLights.emplace_back(glm::vec3(0.0f, 0.0f, -4.0f), mult*3.0f, glm::vec3(1.0f, 0.0f, 0.0f));

			auto& areaLights = scene.getAreaLights();
			
			areaLights.emplace_back(glm::vec3(2.0, 2.0f, 1.0f), glm::vec3(-1.0f, -1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f), 10.0f);
			areaLights.emplace_back(glm::vec3(-2.0, 2.0f, 1.0f), glm::vec3(+1.0f, -1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f), 10.0f);

			scene.getCam().lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			return scene;
		}
	}
	Program::Program() :
		window("Title", w, h),
		renderer(&window.getContext()),
		materialsLibrary(),
		scene(buildTestScene(materialsLibrary)),
		texture(initTexture(w, h)),
		gui(w, h),
		rendering(true)// Start rendering immidietely after a device is set
	{}

	void Program::Run()
	{
		// TEMP Defaults to GPU on my computer
		//renderer.setDevice(&renderer.getAvailablePlatforms()[0].devices[0]);
		while (!window.beginFrame())
		{
			auto beginFrame = std::chrono::high_resolution_clock::now();
			bool began = false;
			if (rendering&& renderer.getCurrentDevice())//A device is set
			{
				if (renderer.isRecompilationRequested())
					renderer.recompile(w, h, texture, scene);
				renderer.beginRender();
				began = true;
			}

			auto[x, y] = window.getWinSize();
			gui.draw(float(x), float(y), *this);

			if (began) renderer.finishRender();

			auto endFrame = std::chrono::high_resolution_clock::now();
			auto frameTime = endFrame - beginFrame;

			std::cout << "\t Frame time:" << frameTime.count() / double(decltype(frameTime)::period::den) * 1000 << "ms\n";
			window.renderFrame();
		}
	}
	glm::vec2 Program::getTextureDims() const
	{
		return { (float)w,(float)h };
	}
	void Program::pauseRendering()
	{
		rendering = false;
	}
	void Program::resumeRendering()
	{
		rendering = true;
	}
	bool Program::isRendering()
	{
		return rendering;
	}
}
