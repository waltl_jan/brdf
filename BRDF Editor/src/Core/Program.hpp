#ifndef BRDFEDITOR_PROGRAM_HEADER
#define BRDFEDITOR_PROGRAM_HEADER

#include <src/GUI/GUI.hpp>
#include <src/OpenCLRenderer/Renderer/Renderer.hpp>
#include <src/OpenCLRenderer/Scene.hpp>

#include "WindowManager.hpp"

namespace brdfEditor
{
	// The main class of this project
	class Program
	{
	public:
		Program();
		// Contains mains while(true) loop
		// Each frame GUI is drawn and, if not paused, renderer is called->one sample is added to the texture.
		void Run();
		// Texture used by the renderer
		GLuint getTextureID() const { return texture; }
		glm::vec2 getTextureDims() const;
		clRenderer::Scene& getScene() { return scene; }
		clRenderer::Renderer& getRenderer() { return renderer; }
		const clRenderer::Renderer& getRenderer() const { return renderer; }
		clRenderer::MaterialsLibrary& getMatLib() { return materialsLibrary; }
		const clRenderer::MaterialsLibrary& getMatLib() const { return materialsLibrary; }
		// Pauses rendering process, still draws GUI
		void pauseRendering();
		void resumeRendering();
		// Whether the rendering is paused or not.
		bool isRendering();
	private:
		WindowManager window;
		clRenderer::Renderer renderer;
		clRenderer::MaterialsLibrary materialsLibrary;
		clRenderer::Scene scene;//Possible multiple scenes
		GLuint texture;
		gui::GUI gui;
		// Whether the rendering is paused or running.
		bool rendering;
	};
}

#endif