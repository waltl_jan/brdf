#ifndef BRDFEDITOR_OPENCL_TRANSFORM_HEADER
#define BRDFEDITOR_OPENCL_TRANSFORM_HEADER

#include <glm/mat4x4.hpp>

namespace brdfEditor::clRenderer
{
	class Transform
	{
	public:
		Transform(const glm::mat4& t);
		//Non-rotated object
		Transform(const glm::vec3 pos);
		const glm::mat4& get() const;
		//THOUGHT get,set? Or just apply?
	private:
		glm::mat4 t;
	};
}
#endif // !BRDFEDITOR_OPENCL_TRANSFORM_HEADER
