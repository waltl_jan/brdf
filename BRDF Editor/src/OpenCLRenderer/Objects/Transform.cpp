#include "Transform.hpp"

namespace brdfEditor::clRenderer
{
	Transform::Transform(const glm::mat4 & t) :
		t(t) {}
	Transform::Transform(const glm::vec3 pos) :
		t(1.0f)
	{
		t[3] = glm::vec4(pos, 1.0f);
	}
	const glm::mat4 & Transform::get() const
	{
		return t;
	}
}
