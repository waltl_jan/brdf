#include "Sphere.hpp"

namespace brdfEditor::clRenderer
{
	Sphere::Sphere(const Transform & t, const MaterialsLibrary::MaterialCHandle & mat, float radius) :
		Object(t, mat), r(radius) {}

	Sphere::clRep Sphere::getCL() const
	{
		auto pos4 = this->t.get()[3];
		return cl_float4{ pos4.x, pos4.y, pos4.z, r };
	}
}

