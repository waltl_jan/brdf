#include "Object.hpp"

namespace brdfEditor::clRenderer
{
	Object::Object(const Transform & t, MaterialsLibrary::MaterialCHandle mat) :
		t(t), mat(mat) {}

	MaterialsLibrary::MaterialCHandle Object::getMatHandle() const
	{
		return mat;
	}
	Transform & Object::getTransform()
	{
		return t;
	}
	const Transform & Object::getTransform() const
	{
		return t;
	}
}
