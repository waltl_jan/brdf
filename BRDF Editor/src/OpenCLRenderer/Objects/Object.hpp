#ifndef BRDFEDITOR_OPENCL_OBJECT_HEADER
#define BRDFEDITOR_OPENCL_OBJECT_HEADER

#include <src/OpenCLRenderer/Materials/MaterialsLibrary.hpp>

#include "Transform.hpp"

namespace brdfEditor::clRenderer
{
	//A renderable unit of a scene with position and material.
	class Object
	{
	public:
		Object(const Transform& t, MaterialsLibrary::MaterialCHandle mat);
		MaterialsLibrary::MaterialCHandle getMatHandle() const;
		Transform& getTransform();
		const Transform& getTransform() const;
	protected://RESOLVE protected or get/set?
		Transform t;
		MaterialsLibrary::MaterialCHandle mat;
	};

}
#endif // !BRDFEDITOR_OPENCL_OBJECT_HEADER
