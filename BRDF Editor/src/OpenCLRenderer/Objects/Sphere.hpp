#ifndef BRDFEDITOR_OPENCL_SPHERE_HEADER
#define BRDFEDITOR_OPENCL_SPHERE_HEADER

#include <src/OpenCLRenderer/CLHeaders.hpp>

#include "Object.hpp"

namespace brdfEditor::clRenderer
{
	class Sphere : public Object
	{
	public:
		using clRep = cl_float4;
		Sphere(const Transform& t, const MaterialsLibrary::MaterialCHandle& mat, float radius);

		clRep getCL() const;
	private:
		float r;
	};
}
#endif // !BRDFEDITOR_OPENCL_OBJECT_HEADER
