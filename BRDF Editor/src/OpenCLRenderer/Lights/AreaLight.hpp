#ifndef BRDFEDITOR_OPENCL_AREA_LIGHT_HEADER
#define BRDFEDITOR_OPENCL_AREA_LIGHT_HEADER

#include <glm/vec3.hpp>

#include <src/OpenCLRenderer/CLHeaders.hpp>

namespace brdfEditor
{
	namespace clRenderer
	{
		class AreaLight
		{
		public:
			struct AreaLight_cl
			{
				cl_float4 pos;
				cl_float4 normal;
				cl_float4 color;
				cl_float intensity;
			};
			using clRep = AreaLight_cl;
			//IMPROVE Physical units for intensity
			AreaLight(const glm::vec3& pos, const glm::vec3& normal, glm::vec3 color, float intensity);

			// Returns CL representation of this light
			clRep getCL() const;
			// TODO Get/set for GUI?
		public:
			glm::vec3 pos, normal, col;
			float intensity;
		};
	}
}
#endif // !BRDFEDITOR_OPENCL_AREA_LIGHT_HEADER
