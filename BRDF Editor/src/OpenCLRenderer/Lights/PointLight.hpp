#ifndef BRDFEDITOR_OPENCL_LIGHT_HEADER
#define BRDFEDITOR_OPENCL_LIGHT_HEADER

#include <glm/vec3.hpp>

#include <src/OpenCLRenderer/CLHeaders.hpp>

namespace brdfEditor::clRenderer
{
	class PointLight
	{
	public:
		struct PointLight_cl
		{
			cl_float4 pos;
			cl_float4 color;
			cl_float intensity;
		};
		using clRep = PointLight_cl;

		PointLight(const glm::vec3& pos, float intensity, glm::vec3 color);
		// Returns CL representation of this light
		clRep getCL() const;
		// TODO Get/set for GUI?
	private:
		glm::vec3 pos, col;
		float intensity;
	};
}
#endif // !BRDFEDITOR_OPENCL_LIGHT_HEADER
