#include "AreaLight.hpp"
#include <glm/geometric.hpp>

namespace brdfEditor::clRenderer
{
	AreaLight::AreaLight(const glm::vec3 & pos, const glm::vec3 & normal, glm::vec3 color, float intensity) :
		pos(pos), normal(normal), col(color), intensity(intensity)
	{
		
		this->normal = glm::normalize(this->normal);
	}
	AreaLight::clRep AreaLight::getCL() const
	{
		return AreaLight_cl{
			cl_float4{pos.x,pos.y,pos.z,0.0f},
			cl_float4{normal.x,normal.y,normal.z,0.0f},
			cl_float4{col.r,col.g,col.b,0.0f},
			cl_float{intensity} };
	}
}
