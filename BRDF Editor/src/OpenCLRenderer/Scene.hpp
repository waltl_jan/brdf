#ifndef BRDFEDITOR_OPENCL_SCENE_HEADER
#define BRDFEDITOR_OPENCL_SCENE_HEADER

#include <vector>
#include <string>
#include "Camera.hpp"
#include "Lights/AreaLight.hpp"
#include "Lights/PointLight.hpp"
#include "Objects/Sphere.hpp"
#include "Objects/TriangleMesh.hpp"

namespace brdfEditor
{
	namespace clRenderer
	{
		class Scene
		{
		public:
			std::vector<AreaLight>& getAreaLights();
			const std::vector<AreaLight>& getAreaLights() const;
			Camera& getCam();
			const Camera& getCam() const;
			std::vector<TriangleMesh>& getMeshes();
			const std::vector<TriangleMesh>& getMeshes() const;
			std::vector<PointLight>& getPointLights();
			const std::vector<PointLight>& getPointLights() const;
			//Returns list of materials currently used in the scene.
			std::vector<MaterialsLibrary::MaterialCHandle> getSceneMaterials() const;
			std::vector<Sphere>& getSpheres();
			const std::vector<Sphere>& getSpheres() const;
		private:
			Camera cam;
			std::vector<Sphere> spheres;
			std::vector<PointLight> pointLights;
			std::vector<AreaLight> areaLights;
			std::vector<TriangleMesh> meshes;
		};
	}
}
#endif // !BRDFEDITOR_OPENCL_SCENE_HEADER
