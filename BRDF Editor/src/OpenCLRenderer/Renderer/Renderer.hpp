#ifndef BRDFEDITOR_OPENCL_RENDERER_HEADER
#define BRDFEDITOR_OPENCL_RENDERER_HEADER

#include <chrono>
#include <cstddef>
#include <memory>

#include <src/Core/Exception.hpp>
#include <src/OpenCLRenderer/CLHeaders.hpp>
#include <src/OpenCLRenderer/Kernel/Kernel.hpp>

#include "PlatformInfo.hpp"
#include "DeviceInfo.hpp"

namespace brdfEditor
{
	class WindowContext;

	namespace clRenderer
	{
		class Scene;

		//Renders the scene using path tracing implemented with OpenCL.
		class Renderer
		{
		public:
			//Statistics about the renderer,mostly for debug now.
			struct Stats
			{
				std::size_t w = 0, h = 0;//Res
				std::size_t numSamples = 0;
				std::size_t renderTimeMili = 0;//RenderTime in miliseconds
			};

			//"How much" must the renderer be recompiled.
			enum class RecompilationFlags :uint8_t
			{
				//No need
				None = 0,
				//Only reset arguments( e.g. The scene changed)
				MaterialsParameters = 1 << 0,
				SceneObjects = 1 << 1,
				Camera = 1<<2,
				BRDFs = 1 << 3,
				Texture=1<<4,
				//Full recompilation, recompiles the kernel source.
				//Needed to apply changes in active materials' BRDFs.
				Full = 0xFF,
			};

			// --winContext: Context of the main window, required for OpenCL-OpenGL interop.
			//				 Is captured, must outlive this instance.
			Renderer(const WindowContext* winContext);
			~Renderer();
			// (Re)sets the rendering processing with new arguments if it was requested.
			// --textureID: OpenGL texture that remains valid for all calls to begin/finishRender().
			// --texWidth,texHeight: Dimensions of the texture represented by textureID.
			// Throws: InvalidRenderingDevice if none is set.
			void recompile(std::size_t texWidth, std::size_t texHeight, GLuint textureID, const Scene& scene);
			// Begins to render next sampling of the scene. Renderer must have been recompiled if it had been requested.
			// Throws: InvalidRenderingDevice if none is set.
			//		   RecompilationNeeded if the recompilation has been requested and not done.
			void beginRender();
			// Finished started rendering, writes result to the texture.
			// Can ONLY be called if beginRender() returned true.
			void finishRender();
			const Stats& getStats() const;
			const std::vector<PlatformInfo>& getAvailablePlatforms() const;
			// Returns currently used OpenCL device for rendering or nullptr.
			const DeviceInfo* getCurrentDevice() const;
			// Sets OpenCL device to be used for rendering.
			// --device: Nullptr to disable rendering.
			//			 pointer must point to a device from getplatforms().
			// If the device have been changed a recompilation is requested.
			void setDevice(const DeviceInfo* device);
			// Signals that the Renderer should be recompiled.
			void requestRecompilation(RecompilationFlags flags);
			RecompilationFlags requestedRecompilation() const;
			// Returns RecompilationFlags!=RecompilationFlags::None.
			bool isRecompilationRequested() const;
			bool isRecompilationRequested(RecompilationFlags flag) const;
		private:
			std::vector<PlatformInfo> platforms;
			const WindowContext* winContext;
			// Pointer into a platform inside platforms.
			const DeviceInfo* currentDevice;
			cl::Context clContext;
			cl::Device clDevice;
			cl::CommandQueue clQueue;
			std::unique_ptr<Kernel> kernel;

			Stats stats;
			std::chrono::steady_clock::time_point renderStart;

			// Renderer must be recompiled in order to be able to render.
			//	- when device is changed.
			//	- when its requested from outside (i.e to reflect changes in the Scene).
			RecompilationFlags recompFlags;
		};
		Renderer::RecompilationFlags operator|(Renderer::RecompilationFlags l, Renderer::RecompilationFlags r);
		Renderer::RecompilationFlags operator&(Renderer::RecompilationFlags l, Renderer::RecompilationFlags r);
		Renderer::RecompilationFlags& operator|=(Renderer::RecompilationFlags& l, Renderer::RecompilationFlags r);
		Renderer::RecompilationFlags& operator&=(Renderer::RecompilationFlags& l, Renderer::RecompilationFlags r);

	}

}

#endif // !BRDFEDITOR_RENDERER_HEADER
