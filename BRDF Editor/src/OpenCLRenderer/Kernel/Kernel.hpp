#ifndef BRDFEDITOR_OPENCL_KERNEL_HEADER
#define BRDFEDITOR_OPENCL_KERNEL_HEADER

#include <memory>
#include <string>

#include <glad/glad.h>
#include <src/OpenCLRenderer/CLHeaders.hpp>
#include <src/OpenCLRenderer/Camera.hpp>

#include "KernelSource.hpp"

namespace brdfEditor::clRenderer
{
	class Scene;

	// Kernel used for path tracing algorithm.
	// Abstract class.
	class Kernel
	{
	public:
		// Creates kernel with given source code.
		// --contex: OpenCL context must outlive this instance.
		Kernel(const cl::Context& context, const cl::Device& dev, KernelMainSource kernelSource);
		virtual ~Kernel() = default;

		// Passes new arguments to the kernel.
		// --textureID: Must remain valid throughout use of these arguments.
		void setArguments(std::size_t texWidth, std::size_t texHeight, GLuint textureID, const Scene& scene);

		void setScene(const Scene& scene);
		void setSceneMaterialParameters(const Scene& scene);
		void setTexture(std::size_t texWidth, std::size_t texHeight, GLuint textureID);
		void setCamera(const Camera& sceneCam);
		void beginExecute(const cl::CommandQueue& queue);
		void finishExecute(const cl::CommandQueue& queue);

		// Builds kernel with new source code. 
		void rebuild(const char* source, const char* kernelFncName);

		// How many times was the scene sampled and therefore how many samples the texture contains.
		cl_int getNumSamples();
	protected:
		cl::Kernel kernel;
		// Image that show rendered scene - buffImage averaged over the number of samples.
		std::unique_ptr<cl::Image> outImage;
		// Size of work to be executed, same as size of the outImage,buffImage
		cl::NDRange globalWork;
		GLuint texture;
	private:
		// Representation of the Scene in the kernel.
		struct Scene_cl
		{
			cl_float4 camPos;
			cl_float4 camDir;
			cl_float4 camUp;
			cl_int numSpheres;
			cl_int numMeshes;
			cl_int numPointLight;
			cl_int numAreaLights;
		};

		virtual void doBeginExecute(const cl::CommandQueue& queue) = 0;
		virtual void doFinishExecute(const cl::CommandQueue& queue) = 0;
		virtual std::unique_ptr<cl::Image> genOutImage(const cl::Context& context, std::size_t texWidth,
			std::size_t texHeight, GLuint textureID) = 0;

		KernelMainSource kernelSource;
		cl::Context context;
		cl::Device device;
		cl::Program program;
		Scene_cl scene;
		// Buffer for each type of object
		cl::Buffer spheres;
		cl::Buffer triangles;
		cl::Buffer meshes;
		cl::Buffer bvhNodes;
		// Stores material ID for reach object in the scene. 
		// [0,spheres.size()) for spheres
		// TODO for the other objects
		cl::Buffer matInfos;
		cl::Buffer matParams;
		cl::Buffer pointLights;
		cl::Buffer areaLights;
		// Image in which renderer stores all samples of the rendered scene
		cl::Buffer buffImage;
		// RNG generator's state for each pixel
		cl::Buffer rngState;
		cl::Image2D skyHDR;
		// Cached vector of random numbers.
		std::vector<cl_uint2> cachedRngVec;
		cl_int numSamples;
	};

	// Kernel that uses a buffer to render into.
	// Buffer is then copied to the texture.
	class BufferedKernel final :public Kernel
	{
	public:
		using Kernel::Kernel;
		virtual ~BufferedKernel() override = default;
	private:
		std::vector<cl_float4> imgBuff;
		virtual void doBeginExecute(const cl::CommandQueue& queue) override final;
		virtual void doFinishExecute(const cl::CommandQueue& queue) override final;
		virtual std::unique_ptr<cl::Image> genOutImage(const cl::Context& context, std::size_t texWidth,
			std::size_t texHeight, GLuint textureID) override final;
	};

	// Kernel that directry renders to the OpenGL texture.
	class InteropKernel final : public Kernel
	{
	public:
		using Kernel::Kernel;
	private:
		virtual void doBeginExecute(const cl::CommandQueue& queue) override final;
		virtual void doFinishExecute(const cl::CommandQueue& queue) override final;
		virtual std::unique_ptr<cl::Image> genOutImage(const cl::Context& context, std::size_t texWidth,
			std::size_t texHeight, GLuint textureID) override final;
	};
}
#endif // ! BRDFEDITOR_OPENCL_KERNEL_HEADER
