#ifndef BRDFEDITOR_OPENCL_KERNEL_SOURCE_HEADER
#define BRDFEDITOR_OPENCL_KERNEL_SOURCE_HEADER

#include <string>
#include <vector>
#include <map>
#include "src/OpenCLRenderer/Materials/material.hpp"

namespace brdfEditor::clRenderer
{

	//Contains source of one BRDF function used in OpenCL kernel as materials.
	class BRDFKernelSource
	{
	public:

		using parameters_t = std::map<std::string, Material::Param>;

		//Source must contain 'BRDF' function and optionally 'SampleBRDF' function.
		//Both function names are prefixed with passed prefix.
		BRDFKernelSource(std::string source, const std::string& prefix);

		//Contains declaration of 'BRDF' and optionally also 'genDir'
		const std::string& getDeclaration()const { return declaration; }
		//Whether the optional 'genDir' function was declared.
		bool hasCustomSampling() const { return !sampleBrdfFncName.empty(); }
		const std::string& getBRDFFncName() const { return brdfFncName; }
		//Returns "" if hasCustomSampling()==false
		const std::string& getSampleBRDFFncName()const { return sampleBrdfFncName; }
		//Returns Definitions for BRDF() and possibly SampleBRDF()
		const std::string& getDefs()const { return definitions; }
		const std::string& getName()const { return name; }
		const std::string& getSource() const { return inSource; }
		const std::string& getSource() { return inSource; }
		void setSource(const std::string& newSource);
		const parameters_t& getParams()const { return parameters; }
		friend bool operator==(const BRDFKernelSource& l, const BRDFKernelSource& r)
		{
			return l.brdfFncName == r.brdfFncName && l.getDefs() == r.getDefs();
		}
	private:
		//TODO parsed header, parameters...
		std::string name;
		std::string inSource;

		std::string brdfFncName;
		std::string sampleBrdfFncName;
		//Header declaration
		std::string declaration;
		//Definitions for BRDF() and possibly SampleBRDF()
		std::string definitions;

		parameters_t parameters;

		//Processes the insource.
		void process();
	};
	//Contains source code for an OpenCL kernel program with the main kernel function.
	class KernelMainSource final
	{
	public:
		using BRDFs_t = std::vector<const BRDFKernelSource*>;
		//Creates the OpenCL kernel source from main kernel entry function and list of BRDF functions.
		//Also creates BRDFEval and BRDFSampleEval functions that evalutes the materials
		//	kernelFncSource - must contain __kernel function with 'kernelFncName' + any helper functions, macros...
		KernelMainSource(const std::string& kernelFncSource, std::string kernelFncName, const BRDFs_t& BRDFs);

		const std::string& getKernelFncName()const;
		//Returns ID fo reach BRDF that was passed in the ctor.
		//Is used for matID for the BRDF switch function.
		int getMatID(const std::string& brdfFncName) const;
		const std::string& getSource() const;
	private:
		std::string source;
		std::map<std::string, int> matIDs;//TODO rename to brdfID
		std::string kernelFncName;
	};
}
#endif //BRDFEDITOR_OPENCL_KERNEL_SOURCE_HEADER
