#include "KernelSource.hpp"
#include "src/Core/Exception.hpp"
#include <regex>
#include <sstream>
#include <cctype>
#include <iostream>
#include <cassert>

namespace brdfEditor::clRenderer
{
	namespace
	{
		std::map<std::string, int> buildMatIds(const KernelMainSource::BRDFs_t& BRDFs)
		{
			std::map<std::string, int> matIDs;
			int i = 0;
			for (auto&& brdf : BRDFs)
				matIDs.try_emplace(brdf->getBRDFFncName(), i++);
			return matIDs;
		}

		constexpr const char* brdfEvalDecl = "float3 BRDFEval(MatInfo matInfo, float3 normal, float3 wIn, float3 wOut, __global Param* paramBuffer)";
		constexpr const char* brdfSampleEvalDecl = "float3 BRDFSampleEval(MatInfo matInfo, float3 normal, float3 wIn, float3* wOut, RNGState* rngState, float* pdf, __global Param* paramBuffer)";
		constexpr const char* beginSwitch = "\n{ \n\tswitch(matInfo.brdfID)\n\t{\n";
		//Tag located in the kernel that marks where to put the declarations
		constexpr const char* declTag = "//DECLHERE";
		//Builds BRDFEval() function for the path tracer that uses switch(matID) to select material's BRDF()
		//	BRDFs - will be used in the switch
		//	matIDs - converts BRDFFncName() to the matID, must contain entries for the BRDFs
		//	declaration, definition - output parameters for the BRDFEval() function.
		void buildBRDFEvalFnc(const KernelMainSource::BRDFs_t& BRDFs, const std::map<std::string, int>& matIDs, std::string* declaration, std::string* definition)
		{
			*declaration = std::string(brdfEvalDecl) + ";\n";

			std::string def;
			def += brdfEvalDecl;
			def += beginSwitch;
			for (auto&& brdf : BRDFs)
			{
				//Will be found, because matIDs were built from BRDFs
				int matID = matIDs.at(brdf->getBRDFFncName());
				std::string matCase = "\tcase " + std::to_string(matID) + ":\n";
				def += matCase;
				def += "\t\treturn " + brdf->getBRDFFncName() + "(normal, wIn, wOut,paramBuffer + matInfo.paramOff);\n";

			}
			def += "\tdefault:\n\t\treturn (float3)(0.0f);\n\t}\n}\n";
			*definition = std::move(def);
		}
		//Builds BRDFSampleEval() function for the path tracer that uses switch(matID) to select material's SampleBRDF().
		//If a BRDF does not support custom sampling, cosine-weighted sampling and material's BRDF() will be used.
		//	BRDFs - will be used in the switch
		//	matIDs - converts BRDFFncName() to the matID, must contain entries for the BRDFs
		//	declaration, definition - output parameters for the BRDFSampleEval() function.
		void builSampledBRDFEvalFnc(const KernelMainSource::BRDFs_t& BRDFs, const std::map<std::string, int>& matIDs, std::string* declaration, std::string* definition)
		{
			*declaration = std::string(brdfSampleEvalDecl) + ";\n";

			std::string def;
			def += brdfSampleEvalDecl;
			def += beginSwitch;
			for (auto&& brdf : BRDFs)
			{
				//Will be found, because matIDs were built from BRDFs
				int matID = matIDs.at(brdf->getBRDFFncName());
				std::string matCase = "\tcase " + std::to_string(matID) + ":\n";
				def += matCase;
				if (brdf->hasCustomSampling())
					def += "\t\treturn " + brdf->getSampleBRDFFncName() + "(normal,wIn,wOut,pdf,rngState,paramBuffer + matInfo.paramOff);\n";
				else
				{
					def += "\t\t*wOut=genCosineDir(wIn, normal, rngState, pdf);\n";
					def += "\t\treturn " + brdf->getBRDFFncName() + "(normal, wIn, *wOut,paramBuffer + matInfo.paramOff);\n";
				}
			}
			def += "\tdefault:\n\t\treturn (float3)(0.0f);\n\t}\n}\n";
			*definition = std::move(def);
		}

		Material::Param::typeE stringToParamType(const std::string & paramType)
		{
			using pT = Material::Param::typeE;
			if (paramType == "float")
				return pT::float1;
			else if (paramType == "float2")
				return pT::float2;
			else if (paramType == "float3")
				return pT::float3;
			else if (paramType == "float4")
				return pT::float4;
			else if (paramType == "int")
				return pT::integer;
			else
				throw Exception("Syntax error: invalid parameter type: " + paramType);
		}
		std::string paramTypeToString(Material::Param::typeE p)
		{
			using pT = Material::Param::typeE;
			switch (p)
			{
			case pT::float1:
				return "f1";
			case pT::float2:
				return "f2";
			case pT::float3:
				return "f3";
			case pT::float4:
				return "f4";
			case pT::integer:
				return "i";
			default:
				assert("Forgot to add enum case form Material::Param::typeE");
				return "";
			}
		}
		std::string paramTypeToTypeString(Material::Param::typeE p)
		{
			using pT = Material::Param::typeE;
			switch (p)
			{
			case pT::float1:
				return "float";
			case pT::float2:
				return "float2";
			case pT::float3:
				return "float3";
			case pT::float4:
				return "float4";
			case pT::integer:
				return "int";
			default:
				assert("Forgot to add enum case form Material::Param::typeE");
				return "";
			}
		}
		BRDFKernelSource::parameters_t buildParameters(const std::string& params)
		{
			const std::regex paramRegex{ R"(^[ \t]*(float|float[234]|int)[ \t]+([a-zA-Z_0-9]+);[ \t]*)" };
			std::istringstream stream(params);
			std::string line;
			std::smatch match;

			BRDFKernelSource::parameters_t parameters;

			while (std::getline(stream, line))
			{
				if (line.empty() || std::all_of(line.begin(), line.end(), [](char c) { return std::isalpha(c); }))
					continue;
				if (!std::regex_search(line, match, paramRegex))
					throw Exception("Syntax error: Invalid parameter");
				else
				{
					Material::Param param;
					std::string opt = match.suffix().str();
					param.isColor = opt.find("[color]") != std::string::npos;

					std::smatch optMatch;
					param.min = 0.0f;
					param.max = 10.0f;
					param.step = 0.1f;
					if (std::regex_search(opt, optMatch, std::regex(R"(\[min=([0-9\.f]+)\])")))
						param.min = std::stof(optMatch[1].str());
					if (std::regex_search(opt, optMatch, std::regex(R"(\[max=([0-9\.f]+)\])")))
						param.max = std::stof(optMatch[1].str());
					if (std::regex_search(opt, optMatch, std::regex(R"(\[step=([0-9\.f]+)\])")))
						param.step = std::stof(optMatch[1].str());
					//TODO extract default= and set .cl

					param.type = stringToParamType(match[1].str());
					
					parameters[match[2].str()] = param;
				}
			}
			return parameters;
		}
		std::string genParamInitialization(const BRDFKernelSource::parameters_t& params)
		{
			std::ostringstream output;
			std::size_t i = 0;
			for (auto&&[k, v] : params)
			{
				output << paramTypeToTypeString(v.type) << ' ' << k << '=';
				output << std::string("paramBuffer[") << i++ << ']';
				output << ".value." << paramTypeToString(v.type) << ";\n";
			}
			return output.str();
		}
	}


	void BRDFKernelSource::process()
	{
		std::string source = inSource;
		const std::string prefix = name + '_';
		std::smatch match;


		//IMPROVE Make more flexible
		//	- add prefix BRDF calls from SampleBRDF and other functions

		std::regex paramsRegex(R"(params\{([^}]*)\})");
		std::string paramInit;

		if (std::regex_search(source, match, paramsRegex) && match.size() == 2)
		{
			parameters = buildParameters(match[1].str());
			paramInit = genParamInitialization(parameters);
			source = match.suffix().str();
		}

		std::regex declRegex{ R"(float3[ ]+()(BRDF\([^\)]+\)))" };
		if (!std::regex_search(source, match, declRegex) || match.size() != 3)
			throw Exception("BRDF's source does not contain declaration for the BRDF");
		else
		{
			declaration = "float3 " + prefix + match[2].str() + ";\n";
			auto pos = declaration.find(')');
			declaration.replace(pos, 1, ", __global Param* paramBuffer)");

			int declOff = match[1].first - source.begin();
			source.replace(match[1].first, match[1].second, prefix);
			brdfFncName = prefix + "BRDF";

			pos = source.find('{', declOff);
			source.replace(pos + 1, 0, '\n' + paramInit);
			pos = source.find(')', declOff);
			source.replace(pos, 1, ", __global Param* paramBuffer)");
		}

		std::regex sampleDeclRegex{ R"(float3[ ]+()(SampleBRDF\([^\)]+\)))" };
		//Search for optional SampleBRDF function
		if (std::regex_search(source, match, sampleDeclRegex) && match.size() == 3)
		{
			auto d = "float3 " + prefix + match[2].str() + ";\n";
			auto pos = d.find(')');
			d.replace(pos, 1, ", __global Param* paramBuffer)");
			declaration += d;

			int declOff = match[1].first - source.begin();
			source.replace(match[1].first, match[1].second, prefix);
			sampleBrdfFncName = prefix + "SampleBRDF";
			pos = source.find('{', declOff);
			source.replace(pos + 1, 0, '\n' + paramInit);
			pos = source.find(')', declOff);
			source.replace(pos, 1, ", __global Param* paramBuffer)");
		}

		definitions = std::move(source);
	}
	BRDFKernelSource::BRDFKernelSource(std::string source, const std::string& prefix)
	{
		inSource = source;
		name = prefix;
		process();
	}
	void BRDFKernelSource::setSource(const std::string& newSource)
	{
		inSource = newSource;
		process();
	}

	//
	KernelMainSource::KernelMainSource(const std::string& mainSource, std::string kernelFncName, const BRDFs_t& BRDFs) :
		matIDs(buildMatIds(BRDFs)), kernelFncName(std::move(kernelFncName))
	{
		std::string brdfDecls;
		for (auto&&BRDF : BRDFs)
		{
			brdfDecls += BRDF->getDeclaration();
			brdfDecls += '\n';
		}
		std::string brdfDefs{ '\n' };
		for (auto&& BRDF : BRDFs)
		{
			brdfDefs += BRDF->getDefs();
			brdfDefs += '\n';
		}
		std::string brdfEvalDecl, brdfEvalDef, brdfSampleEvalDecl, brdfSampleEvalDef;
		buildBRDFEvalFnc(BRDFs, matIDs, &brdfEvalDecl, &brdfEvalDef);
		builSampledBRDFEvalFnc(BRDFs, matIDs, &brdfSampleEvalDecl, &brdfSampleEvalDef);

		source = mainSource;
		//Inserts declarations to predefined position in the kernel.

		source.replace(source.find(declTag), strlen(declTag), brdfDecls + brdfEvalDecl + brdfSampleEvalDecl);
		//Append all other definitions so they can leverage helper functions in the kernel.
		source += brdfSampleEvalDef;
		source += brdfEvalDef;
		source += brdfDefs;
	}

	const std::string& KernelMainSource::getKernelFncName() const
	{
		return kernelFncName;
	}
	int KernelMainSource::getMatID(const std::string & brdfFncName) const
	{
		auto IT = matIDs.find(brdfFncName);
		return IT != matIDs.end() ? IT->second : -1;
	}
	const std::string & KernelMainSource::getSource() const
	{
		return source;
	}

}
