#include "Kernel.hpp"

#include <algorithm>
#include <numeric>
#include <random>

#include <cload/cload.h>
#include <external/stb/stb_image.h>
#include <src/Core/Exception.hpp>
#include <src/OpenclRenderer/Materials/material.hpp>
#include <src/OpenCLRenderer/Lights/AreaLight.hpp>
#include <src/OpenCLRenderer/Lights/PointLight.hpp>
#include <src/OpenCLRenderer/Objects/Sphere.hpp>
#include <src/OpenCLRenderer/Scene.hpp>

namespace brdfEditor
{
	namespace clRenderer
	{
		namespace
		{
			constexpr const std::uint64_t rngSeed = 123;
			constexpr const auto hdrMapFile = "HDR/tiergarten_4k.hdr";

			cl::Image2D createHDRMapBuffer(const cl::Context& context)
			{
				int width, height, n;
				float* pixels = stbi_loadf(hdrMapFile, &width, &height, &n, 4);
				//for (size_t i = 0; i < width*height * 4; ++i)
				//	pixels[i] = 0.0f;
				cl_int err;
				return cl::Image2D(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
					cl::ImageFormat{ CL_RGBA,CL_FLOAT }, (size_t)width, (size_t)height, 0, pixels, &err);
				stbi_image_free(pixels);
			}
			// Uploads spheres to read-only clBuffer
			cl::Buffer createSphereBuffer(const std::vector<Sphere>& spheres, const cl::Context& context)
			{
				if (spheres.empty())
					return cl::Buffer{};
				std::vector<Sphere::clRep> clSpheres(spheres.size());
				std::transform(std::begin(spheres), std::end(spheres), std::begin(clSpheres), [](const Sphere& s) {return s.getCL(); });
				return  cl::Buffer(context, std::begin(clSpheres), std::end(clSpheres), true);
			}

			// Represents a buffer that stores all BRDFs' parameters.
			struct MatParamsBuffer
			{
				using paramOffsets_t = std::map < std::string, std::size_t>;

				//Buffer of Param_cl structures.
				cl::Buffer matParams;
				// Map that for given BRDF's name returns the offset
				// of its first parameter stored in the matParamBuffer.
				paramOffsets_t matParamOffsets;

				MatParamsBuffer(const Scene& s, const cl::Context& context, const KernelMainSource& mainKernel)
				{
					const auto&& sceneMats = s.getSceneMaterials();

					std::size_t numParams = 0;
					for (auto&& m : sceneMats)
						numParams += m->getParams().size();

					std::vector<Param_cl> args{ numParams };


					std::size_t off = 0;
					for (auto && m : sceneMats)
					{
						auto IT = m->getParams().cbegin();
						for (std::size_t i = 0; i < m->getParams().size(); ++i)
						{
							args[off + i] = (IT++)->second.cl;
						}
						matParamOffsets[m->getName()] = off;
						off += m->getParams().size();
					}
					matParams = numParams == 0 ? cl::Buffer{} : cl::Buffer(context, std::begin(args), std::end(args), true);
				}
			};
			// Buffers that store information about materials and their parameters.
			struct MatInfosBuffers
			{
				//Buffer of MatInfo_cl structures.
				cl::Buffer infosBuffer;
				//Buffer of Param_cl structures.
				cl::Buffer paramsBuffer;

				static MatInfosBuffers create(const Scene& s, const cl::Context& context, const KernelMainSource& mainKernel)
				{
					const auto&& sceneMats = s.getSceneMaterials();
					MatParamsBuffer matParams{ s, context, mainKernel };

					const auto& spheres = s.getSpheres();
					//Number of objects in the scene, not just spheres
					std::vector<MatInfo_cl> clMatInfos;

					for (auto&& sp : spheres)
					{
						MatInfo_cl mI;
						mI.brdfID = mainKernel.getMatID(sp.getMatHandle()->getSource().getBRDFFncName());
						mI.paramOff = static_cast<cl_int>(matParams.matParamOffsets[sp.getMatHandle()->getName()]);

						clMatInfos.push_back(mI);
					}
					for (auto&& mesh : s.getMeshes())
					{
						MatInfo_cl mI;
						mI.brdfID = mainKernel.getMatID(mesh.getMatHandle()->getSource().getBRDFFncName());
						mI.paramOff = static_cast<cl_int>(matParams.matParamOffsets[mesh.getMatHandle()->getName()]);

						clMatInfos.push_back(mI);
					}
					MatInfosBuffers buffer;
					buffer.infosBuffer = cl::Buffer(context, std::begin(clMatInfos), std::end(clMatInfos), true);
					buffer.paramsBuffer = matParams.matParams;
					return buffer;
				}
			};
			// Uploads lights to read-only buffer
			cl::Buffer createPointLightBuffer(const std::vector<PointLight>& pLights, const cl::Context& context)
			{
				if (pLights.empty())
					return cl::Buffer{};
				std::vector<PointLight::clRep> clLights(pLights.size());
				std::transform(std::begin(pLights), std::end(pLights), std::begin(clLights), [](const PointLight& l) { return l.getCL(); });
				return cl::Buffer(context, std::begin(clLights), std::end(clLights), true);
			}
			// Uploads lights to read-only buffer
			cl::Buffer createAreaLightBuffer(const std::vector<AreaLight>& aLights, const cl::Context& context)
			{
				if (aLights.empty())
					return cl::Buffer{};
				std::vector<AreaLight::clRep> clLights(aLights.size());
				std::transform(std::begin(aLights), std::end(aLights), std::begin(clLights), [](const AreaLight& l) { return l.getCL(); });
				return cl::Buffer(context, std::begin(clLights), std::end(clLights), true);
			}
			// Generates n uniformly distributed 64-bit values, upper 32bits are in X, lower in Y
			std::vector<cl_uint2> genRNGVec(std::size_t n, std::uint64_t seed)
			{
				std::vector<cl_uint2> rngState(n);
				std::mt19937_64 eng{ seed };
				std::uniform_int_distribution<std::uint64_t> rng;
				for (std::size_t i = 0; i < rngState.size(); ++i)
				{
					std::uint64_t val = rng(eng);
					rngState[i].x = val >> 32L;
					rngState[i].y = static_cast<cl_uint>(val);
				}
				return rngState;
			}
			// --rngState: Is not modified but must be non-const because cl::Buffer requires void* to data.
			cl::Buffer createRNGStateBuffer(std::vector<cl_uint2>& rngState, const cl::Context& context)
			{
				//IMPROVE Can be further sped up by doing this copy using non-blocking queue.enqueueWriteBuffer
				return cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR, rngState.size() * sizeof(cl_float2), rngState.data());
			}

			struct MeshesBuffer
			{
				// Buffer of floats, each triangle is 9 consecutive floats = 3 vertices * (x,y,z)
				cl::Buffer triangles;
				// Buffer of TriangleMesh_cl structures.
				cl::Buffer meshes;
				cl::Buffer bvhNodes;
				static MeshesBuffer create(const std::vector<TriangleMesh>& meshes, const cl::Context& context, std::size_t* numTriangles)
				{
					BVH::clTris clTriangles;
					std::vector<TriangleMesh::TriangleMesh_cl> clMeshes;
					BVH::clNodes clBVHNodes;

					for (auto&& mesh : meshes)
						clMeshes.push_back(mesh.getCL(clTriangles, clBVHNodes));

					*numTriangles = clTriangles.size();

					MeshesBuffer buffer;
					buffer.triangles = clTriangles.empty() ? cl::Buffer{} :
						cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
							clTriangles.size() * sizeof(BVH::Triangle_cl), clTriangles.data());
					buffer.meshes = clMeshes.empty() ? cl::Buffer{} :
						cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
							clMeshes.size() * sizeof(TriangleMesh::TriangleMesh_cl), clMeshes.data());
					buffer.bvhNodes = clBVHNodes.empty() ? cl::Buffer{} :
						cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
							clBVHNodes.size() * sizeof(BVH::Node_cl), clBVHNodes.data());
					return buffer;
				}
			};

		}

		Kernel::Kernel(const cl::Context& context, const cl::Device& dev, KernelMainSource kernelSource)
			:context(context), kernelSource(kernelSource), device(dev)
		{
			rebuild(kernelSource.getSource().c_str(), kernelSource.getKernelFncName().c_str());
			// Won't change for now, so can be in ctor
			skyHDR = createHDRMapBuffer(context);
		}
		void Kernel::setArguments(std::size_t texWidth, std::size_t texHeight, GLuint textureID, const Scene& scene)
		{
			setScene(scene);
			setTexture(texWidth, texHeight, textureID);
			setSceneMaterialParameters(scene);

		}
		void Kernel::setScene(const Scene & scene)
		{
			std::size_t numTriangles;
			// Reuploads the scene
			spheres = createSphereBuffer(scene.getSpheres(), context);
			MeshesBuffer meshesBuffers = MeshesBuffer::create(scene.getMeshes(), context, &numTriangles);
			triangles = meshesBuffers.triangles;
			meshes = meshesBuffers.meshes;
			bvhNodes = meshesBuffers.bvhNodes;
			pointLights = createPointLightBuffer(scene.getPointLights(), context);
			areaLights = createAreaLightBuffer(scene.getAreaLights(), context);

			//Reset scene info
			this->scene.numSpheres = static_cast<cl_int>(scene.getSpheres().size());
			this->scene.numMeshes = static_cast<cl_int>(scene.getMeshes().size());
			this->scene.numPointLight = static_cast<cl_int>(scene.getPointLights().size());
			this->scene.numAreaLights = static_cast<cl_int>(scene.getAreaLights().size());
			setCamera(scene.getCam());


			kernel.setArg(0, spheres);
			kernel.setArg(1, meshes);
			kernel.setArg(2, triangles);
			kernel.setArg(3, bvhNodes);
			kernel.setArg(4, pointLights);
			kernel.setArg(5, areaLights);
			kernel.setArg(8, this->scene);
			// Not setting 8, numSamples now, will be set by execute()
			kernel.setArg(13, skyHDR);
		}
		void Kernel::setSceneMaterialParameters(const Scene & scene)
		{
			numSamples = 0;

			MatInfosBuffers matInfoBuffers = MatInfosBuffers::create(scene, context, kernelSource);
			matInfos = matInfoBuffers.infosBuffer;
			matParams = matInfoBuffers.paramsBuffer;

			kernel.setArg(6, matInfos);
			kernel.setArg(7, matParams);
		}
		void Kernel::setTexture(std::size_t texWidth, std::size_t texHeight, GLuint textureID)
		{
			numSamples = 0;

			std::size_t imgSize = texWidth * texHeight;
			buffImage = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_HOST_NO_ACCESS, imgSize * sizeof(cl_float4));
			texture = textureID;
			//Reset RNGs, recache if needed
			if (cachedRngVec.size() < imgSize)
				cachedRngVec = genRNGVec(imgSize, rngSeed);
			rngState = createRNGStateBuffer(cachedRngVec, context);
			outImage = this->genOutImage(context, texWidth, texHeight, textureID);
			globalWork = cl::NDRange(texWidth, texHeight);

			kernel.setArg(10, buffImage);
			// HACK Can Image2D,ImageGL be reliably passed as Image? They should, because they are just cl_mem
			kernel.setArg(11, *outImage);
			kernel.setArg(12, rngState);
		}
		void Kernel::setCamera(const Camera & sceneCam)
		{
			numSamples = 0;

			auto cam_cl = sceneCam.getCL();
			scene.camDir = cam_cl.dir;
			scene.camPos = cam_cl.pos;
			scene.camUp = cam_cl.up;
			kernel.setArg(8, this->scene);
		}
		void Kernel::beginExecute(const cl::CommandQueue & queue)
		{
			kernel.setArg(9, numSamples++);
			doBeginExecute(queue);
		}
		void Kernel::finishExecute(const cl::CommandQueue & queue)
		{
			doFinishExecute(queue);
		}
		void Kernel::rebuild(const char* source, const char* kernelFncName)
		{
			try
			{
				program = cl::Program{ context, source, false };
				program.build();
			}
			catch (const cl::Error&e)
			{
				std::string str = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
				//TODO UI error modal would be better
				// - do after BRDF editor is completed
				throw Exception("Kernel build error(" + std::to_string(e.err()) + "): " + str);
			}
			kernel = cl::Kernel(program, kernelFncName);
			//TODO check that kernel arguments are compatible with the scene
		}
		cl_int Kernel::getNumSamples()
		{
			return numSamples;
		}
		void BufferedKernel::doBeginExecute(const cl::CommandQueue& queue)
		{
			queue.enqueueNDRangeKernel(kernel, 0, globalWork);

			std::size_t imgSize = globalWork[0] * globalWork[1];
			imgBuff.resize(imgSize);
			cl::size_t<3> region;
			region[0] = globalWork[0];
			region[1] = globalWork[1];
			region[2] = 1;//Z must be 1 for 2D images
			queue.enqueueReadImage(*outImage, CL_FALSE, cl::size_t<3>{}, region, 0, 0, imgBuff.data());
		}
		void BufferedKernel::doFinishExecute(const cl::CommandQueue& queue)
		{
			queue.finish();
			glTextureSubImage2D(texture, 0, 0, 0, globalWork[0], globalWork[1], GL_RGBA, GL_FLOAT, imgBuff.data());
		}
		std::unique_ptr<cl::Image> BufferedKernel::genOutImage(const cl::Context & context, std::size_t texWidth,
			std::size_t texHeight, GLuint textureID)
		{
			return std::make_unique<cl::Image2D>(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
				cl::ImageFormat{ CL_RGBA,CL_FLOAT }, texWidth, texHeight);
		}
		void InteropKernel::doBeginExecute(const cl::CommandQueue& queue)
		{
			glFinish();
			clEnqueueAcquireGLObjects(queue(), 1, &outImage->operator()(), 0, nullptr, nullptr);
			queue.enqueueNDRangeKernel(kernel, 0, globalWork);
			clEnqueueReleaseGLObjects(queue(), 1, &outImage->operator()(), 0, nullptr, nullptr);
		}
		void InteropKernel::doFinishExecute(const cl::CommandQueue& queue)
		{
			auto ret = queue.finish();
			assert(ret == CL_SUCCESS);
		}
		std::unique_ptr<cl::Image> InteropKernel::genOutImage(const cl::Context& context, std::size_t texWidth,
			std::size_t texHeight, GLuint textureID)
		{
			return std::make_unique<cl::ImageGL>(context, CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, texture);
		}
	}
}
