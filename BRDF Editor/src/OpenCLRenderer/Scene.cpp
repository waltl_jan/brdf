#include "Scene.hpp"

#include <algorithm>

namespace brdfEditor::clRenderer
{
	Camera& Scene::getCam()
	{
		return cam;
	}
	const Camera& Scene::getCam() const
	{
		return cam;
	}
	std::vector<AreaLight>& Scene::getAreaLights()
	{
		return areaLights;
	}
	const std::vector<AreaLight>& Scene::getAreaLights() const
	{
		return areaLights;
	}
	std::vector<TriangleMesh>& Scene::getMeshes()
	{
		return meshes;
	}
	const std::vector<TriangleMesh>& Scene::getMeshes() const
	{
		return meshes;
	}
	std::vector<PointLight>& Scene::getPointLights()
	{
		return pointLights;
	}
	const std::vector<PointLight>& Scene::getPointLights() const
	{
		return pointLights;
	}
	std::vector<MaterialsLibrary::MaterialCHandle> Scene::getSceneMaterials() const
	{
		std::vector<MaterialsLibrary::MaterialCHandle> mats;
		//Add unique materials.
		for (const auto& sphere : spheres)
			if (std::find(mats.begin(), mats.end(), sphere.getMatHandle()) == mats.end())
				mats.emplace_back(sphere.getMatHandle());

		return std::vector<MaterialsLibrary::MaterialCHandle>(mats.begin(), mats.end());
	}
	std::vector<Sphere>& Scene::getSpheres()
	{
		return spheres;
	}
	const std::vector<Sphere>& Scene::getSpheres() const
	{
		return spheres;
	}
}
