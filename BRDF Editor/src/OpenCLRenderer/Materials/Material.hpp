#ifndef BRDFEDITOR_OPENCL_MATERIAL_HEADER
#define BRDFEDITOR_OPENCL_MATERIAL_HEADER

#include <string>
#include <map>
#include <memory>

#include <src/OpenCLRenderer/CLHeaders.hpp>

namespace brdfEditor::clRenderer
{
	class BRDFKernelSource;
	
	// OpenCL struct for a BRDF parameter.
	struct Param_cl
	{
		union
		{
			cl_int integer;
			cl_float float1;
			cl_float2 float2;
			cl_float3 float3;
			cl_float4 float4;
		};
	};
	// OpenCL struct for a material.
	struct MatInfo_cl
	{
		cl_int brdfID;
		cl_int paramOff;
	};

	// Material that can be attached to a scene object.
	class Material
	{
	public:
		// A parameter for the BRDF material.
		struct Param
		{
			enum class typeE
			{
				float1,
				float2,
				float3,
				float4,
				integer,//int
			}type;
			Param_cl cl;
			float step, min, max;
			bool isColor;
		};
		using params_t = std::map<std::string, Param>;
		// Creates named material with given source.
		Material(std::string name, std::shared_ptr<BRDFKernelSource> BRDF);
		const BRDFKernelSource& getSource() const;
		const std::string& getName() const;
		const params_t& getParams() const;
		params_t& getParams();
	private:
		std::string name;
		std::shared_ptr<BRDFKernelSource> BRDF;
		params_t params;
	};
}
#endif // !BRDFEDITOR_OPENCL_MATERIAL_HEADER
