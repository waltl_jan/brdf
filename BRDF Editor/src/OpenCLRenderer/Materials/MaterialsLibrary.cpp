#include "MaterialsLibrary.hpp"

#include <algorithm>
#include <fstream>

#include <src/Core/Exception.hpp>
#include <src/OpenCLRenderer/Kernel/KernelSource.hpp>

namespace brdfEditor::clRenderer
{
	namespace
	{	
		// Loads BRDF source from a file and adds it as a material to the library.
		// --lib: Adds the loaded material here
		// --name: Name of the new material and BRDFSource.
		// --filename: Including path and the extension.
		void loadFromFile(MaterialsLibrary& lib, std::string name, const std::string& filename)
		{
			std::ifstream BRDFSourceFile{ filename };
			std::string source((std::istreambuf_iterator<char>{BRDFSourceFile}), std::istreambuf_iterator<char>{});

			lib.createMaterial(name, source);
		}
	}
	MaterialsLibrary::MaterialsLibrary()
	{
		loadFromFile(*this, "BlinnPhong", "BRDF/BlinnPhong.cl");
		loadFromFile(*this, "CookTorrance", "BRDF/CookTorrance.cl");
		loadFromFile(*this, "Lambert", "BRDF/Lambert.cl");
		loadFromFile(*this, "Mirror", "BRDF/Mirror.cl");
	}
	void MaterialsLibrary::createMaterial(std::string name, std::shared_ptr<BRDFKernelSource> matSource)
	{
		//TODO search vec to ensure it is present here
		materials.emplace_back(std::move(name), std::move(matSource));
	}
	void MaterialsLibrary::createMaterial(std::string name, BRDFKernelSource matSource)
	{
		auto sPtr = std::make_shared<BRDFKernelSource>(std::move(matSource));
		matSources.push_back(sPtr);
		createMaterial(std::move(name), std::move(sPtr));
	}
	void MaterialsLibrary::createMaterial(std::string name, std::string kernelSource)
	{
		auto sPtr = std::make_shared<BRDFKernelSource>(std::move(kernelSource),name);
		matSources.push_back(sPtr);
		createMaterial(std::move(name), std::move(sPtr)); 
	}
	namespace
	{
		// Returns first material with passed name.
		// Templated to allow both cons and non-const versions.
		template<typename MatLib> 
		auto getMatIT(MatLib&& matLib, const std::string& name)
		{
			auto IT = std::find_if(matLib.begin(), matLib.end(), [&n = name](const Material& m) {return m.getName() == n; });
			if (IT == matLib.end())
				throw Exception("Material with the name \"" + name + "\" is not present in the library.");
			else
				return IT;
		}
	}
	MaterialsLibrary::MaterialCHandle MaterialsLibrary::getMaterial(const std::string & name) const
	{
		return MaterialCHandle(getMatIT(materials, name));
	}
	MaterialsLibrary::MaterialHandle MaterialsLibrary::getMaterial(const std::string & name)
	{
		return MaterialHandle(getMatIT(materials, name));
	}
	MaterialsLibrary::MaterialCHandle MaterialsLibrary::begin() const
	{
		return MaterialCHandle{ materials.begin() };
	}
	MaterialsLibrary::MaterialCHandle MaterialsLibrary::end() const
	{
		return MaterialCHandle{ materials.end() };
	}
	MaterialsLibrary::MaterialCHandle MaterialsLibrary::cbegin() const
	{
		return MaterialCHandle{ materials.cbegin() };
	}
	MaterialsLibrary::MaterialCHandle MaterialsLibrary::cend() const
	{
		return MaterialCHandle{ materials.cend() };
	}
	MaterialsLibrary::MatSourceCHandle MaterialsLibrary::beginS() const
	{
		return MatSourceCHandle{ matSources.begin() };
	}
	MaterialsLibrary::MatSourceCHandle MaterialsLibrary::endS() const
	{
		return MatSourceCHandle{ matSources.end() };
	}
	MaterialsLibrary::MatSourceCHandle MaterialsLibrary::cbeginS() const
	{
		return MatSourceCHandle{ matSources.cbegin() };
	}
	MaterialsLibrary::MatSourceCHandle MaterialsLibrary::cendS() const
	{
		return MatSourceCHandle{ matSources.cend() };
	}
}
