#include "Material.hpp"

#include <cassert>
#include <fstream>

#include <src/OpenCLRenderer/Kernel/KernelSource.hpp>

namespace brdfEditor::clRenderer
{
	namespace
	{
		void InitParam(Material::Param &param)
		{
			using t = Material::Param::typeE;
			switch (param.type)
			{
			case t::float1:
				param.cl.float1 = param.min;
				break;
			case t::float2:
				param.cl.float2 = param.isColor ? cl_float2{ 1.0f, 0.0f } : cl_float2{ param.min,param.min };
				break;
			case t::float3:
				param.cl.float3 = param.isColor ? cl_float3{ 1.0f, 1.0f, 1.0f } : cl_float3{ param.max,param.max,param.max };
				break;
			case t::float4:
				param.cl.float4 = param.isColor ? cl_float4{ 1.0f, 0.0f, 0.0f,1.0f } : cl_float4{ param.min,param.min,param.min,param.min };
				break;
			}
		}

	}
	Material::Material(std::string name, std::shared_ptr<BRDFKernelSource> BRDF) :
		name(std::move(name)), BRDF(std::move(BRDF))
	{
		//Creates material from their BRDF description.
		for (auto&& p : this->BRDF->getParams())
		{
			Param param = p.second;
			InitParam(param);
			params[p.first] = param;
		}
	}
	const BRDFKernelSource & Material::getSource() const
	{
		assert(BRDF || "Someone reset BRDFSource pointer, should not have happened.");
		return *BRDF;
	}
	const std::string& Material::getName()const
	{
		return name;
	}

	const Material::params_t& Material::getParams() const
	{
		return params;
	}
	Material::params_t& Material::getParams()
	{
		return params;
	}

}
