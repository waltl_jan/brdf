#ifndef BRDFEDITOR_OPENCL_MATERIALS_LIBRARY_HEADER
#define BRDFEDITOR_OPENCL_MATERIALS_LIBRARY_HEADER

#include <map>
#include <string>

#include "Material.hpp"

namespace brdfEditor::clRenderer
{
	// Collection of materials and their sources.
	class MaterialsLibrary
	{
	private:
		using materials_t = std::vector<Material>;
		// BRDF sources for the materials.
		using matSources_t = std::vector<std::shared_ptr<BRDFKernelSource>>;
	public:
		// Fills the library with basic materials.
		MaterialsLibrary();
		// Small value type, cheap copy, move. random-access iterator.
		// Valid until the material(or the library) exists.
		using MaterialHandle = materials_t::iterator;
		// See MaterialHandle
		using MaterialCHandle = materials_t::const_iterator;
		// Small value type, cheap copy, move. random-access iterator.
		// Valid until the source(or the library) exists.
		using MatSourceCHandle = matSources_t::const_iterator;
		//See MatSourceCHandle
		using MatSourceHandle = matSources_t::const_iterator;
		// Creates material from already present source.
		// --matSource: Must be obtained and present in this instance's materials.
		void createMaterial(std::string name, std::shared_ptr<BRDFKernelSource> matSource);
		// Adds new material and its source to the library.
		void createMaterial(std::string name, BRDFKernelSource matSource);
		// Add new material and its source to the library.
		// Material source is also created with the same name.
		void createMaterial(std::string name, std::string kernelSource);
		// Returns handle to the first material with given name.
		// The handle is valid until this library exists and the material has not been removed.
		// Throws Exception if the material with this name is not present in the library.
		MaterialCHandle getMaterial(const std::string& name) const;
		// See const overload.
		MaterialHandle getMaterial(const std::string& name);
		// Iterating over the materials
		MaterialCHandle begin() const;
		MaterialCHandle end() const;
		MaterialCHandle cbegin() const;
		MaterialCHandle cend() const;

		// Iterating over the material sources.
		MatSourceCHandle beginS() const;
		MatSourceCHandle endS() const;
		MatSourceCHandle cbeginS() const;
		MatSourceCHandle cendS() const;

	private:
		materials_t materials;
		matSources_t matSources;
	};
}
#endif // !BRDFEDITOR_OPENCL_MATERIALS_MANAGER
/*
Material holds weakPtr to BRDFSource+ values of params
BRDFSource hold string source
Lib holds sorted vec sharedptrs to sources
	holds sorted vec materials
Add names to source
*/
