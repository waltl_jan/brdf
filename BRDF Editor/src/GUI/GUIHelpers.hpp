#ifndef BRDFEDITOR_GUI_HELPERS_HEADER
#define BRDFEDITOR_GUI_HELPERS_HEADER

#include <external/ImGui/imgui.h>

#include "GUIParams.hpp"

namespace brdfEditor::gui
{
	constexpr const ImGuiWindowFlags menuNoTitle = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoTitleBar;

	// Draws two side-by-side panels covering the main window
	// --leftPortion: [0,1] width of the left panel
	// --leftContent,rightContent: void(void) callables callend in each window.
	template<typename LeftContentFnc, typename RightContentFnc>
	void drawTwoPanels(const GUIParams& gP, float leftPortion, LeftContentFnc&& leftContent, RightContentFnc&& rightContent,
		ImGuiWindowFlags leftFlags = menuNoTitle, ImGuiWindowFlags rightFlags = menuNoTitle)
	{
		auto leftPos = gP.mainWin.off;
		auto leftSize = gP.mainWin.size;
		leftSize.x *= leftPortion;
		ImGui::SetNextWindowPos(leftPos, ImGuiCond_Always);
		ImGui::SetNextWindowSize(leftSize, ImGuiCond_Always);
		if (ImGui::Begin("LeftPanel", nullptr, ImGuiWindowFlags_NoTitleBar))
			leftContent();
		ImGui::End();

		auto rightPos = gP.mainWin.off;
		rightPos.x += leftSize.x + 5.0f;
		auto rightSize = gP.mainWin.size;
		rightSize.x *= 1.0f - leftPortion;

		ImGui::SetNextWindowPos(rightPos, ImGuiCond_Always);
		ImGui::SetNextWindowSize(rightSize, ImGuiCond_Always);
		if (ImGui::Begin("RightPanel", nullptr, ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoTitleBar))
			rightContent();
		ImGui::End();
	}

	// Draws the title in the menu bar of a window. 
	inline void drawTitle(const char* str)
	{
		if (ImGui::BeginMenuBar())
		{
			ImGui::Text(str);
			ImGui::EndMenuBar();
		}
	}
	// Draws custom content in the menu bar of a window.
	// Offers more flexibility than one string.
	// --contentFnc: void(void) callable.
	template<typename ContentFnc>
	void drawTitle(ContentFnc&& contentFnc)
	{
		if (ImGui::BeginMenuBar())
		{
			contentFnc();
			ImGui::EndMenuBar();
		}
	}
}
#endif // ! BRDFEDITOR_GUI_HELPERS_HEADER
