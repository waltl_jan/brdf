#include "SceneRendering.hpp"

#include <functional>
#include <sstream>

#include <glm/trigonometric.hpp>
#include <ImGui/imgui.h>
#include <src/Core/Program.hpp>

#include "GUI.hpp"
#include "GUIHelpers.hpp"

namespace brdfEditor::gui
{
	using Platform = clRenderer::PlatformInfo;
	using Platforms = std::vector<Platform>;
	using Device = clRenderer::DeviceInfo;
	using Devices = std::vector<Device>;
	namespace
	{
		// Colors representing support for OpenCL platforms and devices in RenderSettings window.
		const ImVec4 okCol{ 0.11f,0.71f,0.03f,1.0f };
		// Limited support.
		const ImVec4 limCol{ 0.98f,0.46f,0.01f,1.0f };
		const ImVec4 errCol{ 1.0f, 0.0f ,0.0f ,1.0f };
		// Draws a selectable with colored label and tooltip on hover.
		// --TooltipFnc: No parameters, return type is ignored.
		template<typename TooltipFnc>
		bool colHovSelectable(const char* label, TooltipFnc&& tooltipFnc, ImVec4 col, bool selected)
		{
			auto&& cPos = ImGui::GetCursorPos();
			ImGui::PushID(label);
			bool ret = ImGui::Selectable("##selectable", selected);
			if (ImGui::IsItemHovered())
				tooltipFnc();
			ImGui::PopID();
			ImGui::SetCursorPos(cPos);
			ImGui::TextColored(col, label);
			return ret;
		}
		// Tooltip drawn when hovering over a platform in platforms' combo in renderer settings.
		void drawComboPlatformTooltip(const Platform& platform)
		{
			ImGui::BeginTooltip();
			if (platform.versionOK)
				ImGui::TextColored(okCol, platform.clVersion.c_str());
			else
				ImGui::TextColored(errCol, (platform.clVersion + " => CANNOT SELECT").c_str());
			if (platform.interopAvail)
				ImGui::TextColored(okCol, "Interop available");
			else
				ImGui::TextColored(limCol, "Interop not avaialble. Buffered approach will be used.");
			ImGui::EndTooltip();
		}
		// Allows choosing a platform from available platform.
		// If a different platform is selected then currPlatform is updated and resets renderer's device to nullptr.
		// Might set currPlatform to nullptr
		void drawSettingsPlatformsCombo(const Platform*& currPlatform, const Platforms& platforms, clRenderer::Renderer& renderer)
		{
			const char* currPlatName = currPlatform ? currPlatform->name.c_str() : "None";
			if (ImGui::BeginCombo("Platforms", currPlatName))
			{
				for (auto&& p : platforms)
				{

					bool selected = currPlatform ? currPlatform->clPlatform() == p.clPlatform() : false;
					const ImVec4& col = p.versionOK ? (p.interopAvail ? okCol : limCol) : errCol;
					// Only select non-selected platforms
					ImGui::PushID(p.clPlatform());

					if (colHovSelectable(p.name.c_str(), [&p = p]() {drawComboPlatformTooltip(p); }, col, selected) && !selected)
					{
						// Only select supported platforms
						currPlatform = p.versionOK ? &p : nullptr;
						// Reset the device
						renderer.setDevice(nullptr);
					}
					ImGui::PopID();
				}
				ImGui::EndCombo();
			}
		}
		// Tooltip drawn when hovering over a device in devices' combo in renderer settings.
		void drawComboDeviceTooltip(const Device& dev)
		{
			ImGui::BeginTooltip();
			if (dev.versionOK)
				ImGui::TextColored(okCol, dev.clVersion.c_str());
			else
				ImGui::TextColored(errCol, (dev.clVersion + " => CANNOT SELECT").c_str());
			ImGui::TextColored(okCol, dev.type == Device::GPU ? "GPU" : "CPU");
			if (dev.interopOK)
				ImGui::TextColored(okCol, "Interop available");
			else
				ImGui::TextColored(limCol, "Interop not avaialble. Buffered approach will be used.");
			ImGui::EndTooltip();
		}
		// Allows choosing a device from available devices.
		// If a different device is selected then sets it in renderer an updates currDevice.
		// Might set currDevice to nullptr
		void drawSettingsDevicesCombo(const Platform& currPlatform, const Device*& currDevice, clRenderer::Renderer& renderer)
		{
			const char* currDeviceName = currDevice ? currDevice->name.c_str() : "None";
			if (ImGui::BeginCombo("Devices", currDeviceName))
			{
				for (auto&& d : currPlatform.devices)
				{

					bool selected = currDevice ? currDevice->clDevice() == d.clDevice() : false;
					const ImVec4& col = d.versionOK ? (d.interopOK ? okCol : limCol) : errCol;
					//Only select non-selected devices
					if (colHovSelectable(d.name.c_str(), [&d = d]() {drawComboDeviceTooltip(d); }, col, selected) && !selected)
						//Only select supported devices
						renderer.setDevice(currDevice = d.versionOK ? &d : nullptr);
				}
				ImGui::EndCombo();
			}
		}
		// Content of a child window for displaying the settings of passed renderer
		void drawRenderSettings(Program& p)
		{
			using namespace clRenderer;
			Renderer& renderer = p.getRenderer();
			// IMPROVE remove static variable, but not so important as ImGui is singleton anyway
			// Used to track selected platform when no device is selected yet.
			const static Platform* currPlatform = nullptr;

			drawTitle("Renderer Settings");

			const Platforms& platforms = renderer.getAvailablePlatforms();
			auto&& currDevice = renderer.getCurrentDevice();
			//Assign if a device is selected
			currPlatform = currDevice ? currDevice->platform : currPlatform;

			drawSettingsPlatformsCombo(currPlatform, platforms, renderer);
			//Change in platform resets the current device
			currDevice = renderer.getCurrentDevice();
			if (currPlatform)
				drawSettingsDevicesCombo(*currPlatform, currDevice, renderer);
			auto&& stats = renderer.getStats();
			int xy[2] = { (int)stats.w,(int)stats.h };
			ImGui::SliderInt2("Resolution", xy, 1, 800);


			const char* renderingState;
			ImVec4 stateCol;
			const char* buttonText;
			std::function<void(void)> action;
			if (!currDevice)
			{
				renderingState = "No device selected";
				stateCol = errCol;
				buttonText = "";
				action = []() {};
			}
			else if (!p.isRendering())
			{
				renderingState = "Stopped";
				stateCol = limCol;
				buttonText = "Resume";
				action = [&p = p]() {p.resumeRendering(); };
			}
			else
			{
				renderingState = "Rendering";
				stateCol = okCol;
				buttonText = "Pause";
				action = [&p = p]() {p.pauseRendering(); };
			}

			ImGui::Text("Rendering state:");
			ImGui::SameLine();
			ImGui::TextColored(stateCol, renderingState);
			ImGui::SameLine();
			if (ImGui::SmallButton(buttonText))
				action();

			// TODO Doesn't belong here, move to renderer and create the concept of max samples.
			static int maxSamples = 5000;
			ImGui::InputInt("Max samples", &maxSamples);
			if (stats.numSamples > maxSamples)
				p.pauseRendering();
		}
		// Controls the rendered scene with trackbal
		// --imgPos: Pixel coordinates of the top-left corner of the image
		// --imRes: Pixel resolution of the image
		void TrackballControl(SceneRendering::Trackball& trackball, clRenderer::Camera& cam, clRenderer::Renderer& renderer, glm::vec2 &imgPos, glm::vec2 &imRes)
		{
			// User just started dragging
			if (!trackball.isDragging && ImGui::IsMouseDragging() && ImGui::IsItemHovered())
			{
				trackball.isDragging = true;
				//Cache the camera
				trackball.cachedCamera = cam;
				trackball.invView = cam.getInvView();
			}
			// User just stopped dragging
			else if (!ImGui::IsMouseDragging())
			{
				trackball.isDragging = false;
			}

			if (trackball.isDragging)
			{
				glm::vec2 mouseDrag = ImGui::GetMouseDragDelta();
				glm::vec2 currMPos = ImGui::GetMousePos();
				glm::vec2 origMPos = currMPos - mouseDrag;
				// From pixel coords to [0,1] on the image.
				currMPos = 2.0f*(currMPos - imgPos) / imRes - glm::vec2(1.0f);
				origMPos = 2.0f*(origMPos - imgPos) / imRes - glm::vec2(1.0f);
				// Y is up.
				currMPos.y *= -1.0f;
				origMPos.y *= -1.0f;
				// Project vectors on the front of the trackball sphere.
				float z1 = std::sqrt(std::max(1.0f - glm::length(origMPos), 0.0f));
				float z2 = std::sqrt(std::max(1.0f - glm::length(currMPos), 0.0f));
				glm::vec3 spOrig{ origMPos,z1 };
				glm::vec3 spCurr{ currMPos,z2 };

				spOrig = glm::normalize(spOrig);
				spCurr = glm::normalize(spCurr);

				float cosAngle = glm::dot(spOrig, spCurr);
				auto axis = glm::normalize(glm::cross(spCurr, spOrig));
				auto worldAxis = glm::vec3(trackball.invView*glm::vec4(axis, 0.0f));
				// Set the camera to the one at the start of the dragging
				// and rotate it accordingly.
				const auto& c = trackball.cachedCamera;
				cam.lookAt(c.getPos(), c.getTargetPos(), c.getUpDir());
				cam.rotateAround(worldAxis, glm::acos(cosAngle));
				renderer.requestRecompilation(clRenderer::Renderer::RecompilationFlags::Camera);
			}
		}

		// Returns whether any material changed.
		bool drawMatParams(clRenderer::Material& mat)
		{
			bool changed = false;
			drawTitle(mat.getName().c_str());
			clRenderer::Material::params_t& params = mat.getParams();
			for (auto&&[paramName, param] : params)
			{
				using pt = decltype(param.type);
				float tmp[4];
				bool t;
				switch (param.type)
				{
				case pt::float1:
					changed |= ImGui::SliderFloat(paramName.c_str(), &param.cl.float1, param.min, param.max);
					break;
				case pt::float2:
					tmp[0] = param.cl.float2.x;
					tmp[1] = param.cl.float2.y;
					if (ImGui::SliderFloat2(paramName.c_str(), tmp, param.min, param.max))
					{
						changed = true;
						param.cl.float2 = cl_float2{ tmp[0], tmp[1] };
					}
					break;
				case pt::float3:
					tmp[0] = param.cl.float3.x;
					tmp[1] = param.cl.float3.y;
					tmp[2] = param.cl.float3.z;
					t = param.isColor ? ImGui::ColorEdit3(paramName.c_str(), tmp) : ImGui::SliderFloat3(paramName.c_str(), tmp, param.min, param.max);
					if (t)
					{
						changed = true;
						param.cl.float3 = cl_float3{ tmp[0], tmp[1],tmp[2] };
					}
					break;
				case pt::float4:
					tmp[0] = param.cl.float4.x;
					tmp[1] = param.cl.float4.y;
					tmp[2] = param.cl.float4.z;
					tmp[3] = param.cl.float4.w;
					t = param.isColor ? ImGui::ColorEdit4(paramName.c_str(), tmp) : ImGui::SliderFloat4(paramName.c_str(), tmp, param.min, param.max);
					if (t)
					{
						changed = true;
						param.cl.float4 = cl_float4{ tmp[0], tmp[1],tmp[2],tmp[3] };
					}
					break;
				case pt::integer:
					changed |= ImGui::DragInt(paramName.c_str(), &param.cl.integer);
				default:
					assert("Forgot to add ParamType");
					break;
				}
			}
			return changed;
		}

		// Renders a list of scene materials with their parameters.
		// Offers UI elements to change the parameters and if any do change,
		// then requests the renderer's recompilation.
		void drawSceneMaterials(Program& p)
		{
			drawTitle("Scene materials");
			bool changed = false;
			for (auto && sceneMat : p.getScene().getSceneMaterials())
			{
				// Cannot directly use sceneMat because its constant
				auto& mat = p.getMatLib().getMaterial(sceneMat->getName());

				if (ImGui::BeginChild(mat->getName().c_str(), { 0,100 }, true, ImGuiWindowFlags_MenuBar))
					changed |= drawMatParams(*mat);
				ImGui::EndChild();
			}
			if (changed)
				p.getRenderer().requestRecompilation(clRenderer::Renderer::RecompilationFlags::MaterialsParameters);
		}

		void drawLeftPanel(Program& p)
		{
			// TODO make size relative
			if (ImGui::BeginChild("RendererSettingsChild", { 0,145 }, true, ImGuiWindowFlags_MenuBar))
				drawRenderSettings(p);
			ImGui::EndChild();

			if (ImGui::BeginChild("Scene overviewChild", { 0,300 }, true, ImGuiWindowFlags_MenuBar))
			{
				drawTitle("Scene overview");
				int i = 0;
				bool changed = false;
				for (auto&& s : p.getScene().getAreaLights())
				{
					auto& pos = s.pos;

					ImGui::PushID(++i);
					if (ImGui::BeginChild(i, { 0,100 }, true, ImGuiWindowFlags_MenuBar))
					{
						drawTitle("AreaLight");
						ImGui::Text("Pos: %.2f %.2f %.2f", pos.x, pos.y, pos.z);

						changed |= ImGui::ColorEdit3("Color", &s.col.x);
						changed |= ImGui::SliderFloat("Intensity", &s.intensity, 0.0f, 100.0f);
						//ImGui::Text("Pos: %.2f %.2f %.2f", pos.x, pos.y, pos.z);
					}
					ImGui::EndChild();
					ImGui::PopID();
				}
				if (changed)
					p.getRenderer().requestRecompilation(clRenderer::Renderer::RecompilationFlags::SceneObjects);
			}
			ImGui::EndChild();
			if (ImGui::BeginChild("Scene materials", { 0,0 }, true, ImGuiWindowFlags_MenuBar))
				drawSceneMaterials(p);
			ImGui::EndChild();

		}
		void drawRightPanel(SceneRendering::Trackball& trackball, Program& p)
		{
			if (ImGui::BeginMenuBar())
			{
				// TODO implement
				auto&& stats = p.getRenderer().getStats();

				auto sampleTime = stats.numSamples ? stats.renderTimeMili / (double)stats.numSamples : 0;
				ImGui::Text("Samples: %d/5000 \t Rendering time: %dm:%.2fs \t Sample time: %dm:%.2fs \t Resolution: %dx%d",
					stats.numSamples,
					stats.renderTimeMili / 60000,
					double((stats.renderTimeMili / 1000) % 60),
					int(sampleTime) / 60000,
					sampleTime / 1000.0,
					stats.w, stats.h);
				ImGui::EndMenuBar();
			}
			// RESOLVE renderer aspect ratio
			glm::vec2 imRes{ 800.0f,800.0f };
			glm::vec2 cursorPos = ImGui::GetCursorScreenPos();
			ImGui::Image(ImTextureID(p.getTextureID()), imRes);
			TrackballControl(trackball, p.getScene().getCam(), p.getRenderer(), cursorPos, imRes);
		}
	}
	SceneRendering::SceneRendering(const GUIParams & gP) :
		gP(gP) {}

	void SceneRendering::draw(Program& p)
	{

		auto leftPanel = [&p = p]() {drawLeftPanel(p); };
		auto rightPanel = [&p = p, &t = trackball]() {drawRightPanel(t, p); };
		drawTwoPanels(gP, 0.3f, leftPanel, rightPanel, ImGuiWindowFlags_NoTitleBar);
	}

}
/*
Print scene objects
	- sliders for position
Fix max samples
Fix clock
Fix resolution
Move camera
Zkusit textedit lib
*/
