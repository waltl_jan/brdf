#include "GUI.hpp"

#include <ImGui/imgui.h>
#include <src/Core/Program.hpp>

#include "BRDFTextEditor.hpp"
#include "GUIParams.hpp"
#include "SceneRendering.hpp"

namespace brdfEditor::gui
{
	namespace
	{
		void updateGUIParams(GUIParams& gP, float w, float h)
		{
			// Original screen resolution while developing.
			constexpr const glm::vec2 orig{ 1920.0f,1080.0f };

			const glm::vec2 curr{ w,h };

			auto rescaleVec = [&](const glm::vec2& vec) { return vec * curr / orig; };

			gP.res = curr;

			// Leave space for the main menu bar
			// RESOLVE Probably should not be rescaled as ImGui is already initialized with correct displaySize.
			gP.mainWin.off = { 0.0f,ImGui::GetFrameHeight() };
			//R est can be used.
			gP.mainWin.size = rescaleVec(orig) - gP.mainWin.off;
		}
		GUIParams buildGUIParams(float w, float h)
		{
			GUIParams gP;
			updateGUIParams(gP, w, h);
			return gP;
		}
	}

	GUI::GUI(float w, float h) :
		gParams(buildGUIParams(w, h)),
		sceneRendering(std::make_unique<SceneRendering>(gParams)),
		brdfTextEditor(std::make_unique<BRDFTextEditor>(gParams)),
		selectedWindow(Window::renderedScene) {}

	GUI::~GUI() {}

	void GUI::draw(float w, float h, Program& p)
	{
		updateGUIParams(gParams, w, h);
		drawMenuBar(p);
		switch (selectedWindow)
		{
		case brdfEditor::gui::GUI::Window::renderedScene:
			sceneRendering->draw(p);
			break;
		case brdfEditor::gui::GUI::Window::brdfTextEditor:
			brdfTextEditor->draw(p);
			break;
		default:
			// Forgot to add an enum
			assert(0);
			break;
		}
	}
	void GUI::drawMenuBar(Program& p)
	{
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::Button("Scene Rendering")) selectedWindow = Window::renderedScene;
			ImGui::SameLine();
			ImGui::Text("Scene Editor");
			ImGui::SameLine();
			if (ImGui::Button("BRDF Editor"))
			{
				p.pauseRendering();
				selectedWindow = Window::brdfTextEditor;
			}
			ImGui::SameLine();
			ImGui::Text("BRDF Graphs");
			ImGui::SameLine();
			ImGui::EndMainMenuBar();
		}
	}
}