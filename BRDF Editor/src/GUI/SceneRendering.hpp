#ifndef BRDFEDITOR_GUI_SCENE_RENDERING_HEADER
#define BRDFEDITOR_GUI_SCENE_RENDERING_HEADER

#include <glm/mat4x4.hpp>
#include <src/OpenCLRenderer/Camera.hpp>
#include <src/OpenCLRenderer/Renderer/Renderer.hpp>

namespace brdfEditor
{
	class Program;
	namespace gui
	{
		struct GUIParams;

		// Draws user interface containing the scene and tools to manipulate it and materials.
		class SceneRendering
		{
		public:
			// --gP: Captured, must outlive this instance. 
			SceneRendering(const GUIParams& gP);
			void draw(Program& p);
			struct Trackball//Data for trackball control.
			{
				bool isDragging = false;
				clRenderer::Camera cachedCamera;
				glm::mat4 invView;
			};
		private:

			const GUIParams & gP;
			Trackball trackball;
		};
	}
}
#endif // ! BRDFEDITOR_GUI_SCENE_RENDERING_HEADER
