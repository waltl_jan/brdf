#include "BRDFTextEditor.hpp"

#include <algorithm>
#include <optional>

#include <src/Core/Program.hpp>

#include "GUIHelpers.hpp"

namespace brdfEditor::gui
{
	namespace
	{
		using namespace clRenderer;
		BRDFTextEditor::optSelected drawBRDFList(clRenderer::MaterialsLibrary& matLib,
			const BRDFTextEditor::optSelected& selected)
		{
			BRDFTextEditor::optSelected clicked;
			if (ImGui::BeginChild("Scene overviewChild", { 0,300 }, true, ImGuiWindowFlags_MenuBar))
			{
				drawTitle("BRDF functions");
				for (auto&& handle = matLib.beginS(); handle != matLib.endS(); ++handle)
				{
					if (ImGui::Selectable((**handle).getName().c_str(), selected && *selected == handle,
						ImGuiSelectableFlags_SpanAllColumns))
						clicked = handle;
				}
				ImGui::EndChild();
			}
			return clicked;
		}

		void drawLeftPanel(clRenderer::MaterialsLibrary& matLib, BRDFTextEditor::State& state, Program& p)
		{
			if (auto&& newSel = drawBRDFList(matLib, state.selected))
			{
				state.selected = newSel;
				auto&& source = (**state.selected.value()).getSource();


				std::copy(source.cbegin(), source.cend(), state.textBuffer.begin());
				state.textBuffer[source.length()]='\0';
			}

			if (state.selected.has_value() && ImGui::Button("Recompile"))
			{
				(**state.selected)->setSource(state.textBuffer.data());
				p.getRenderer().requestRecompilation(brdfEditor::clRenderer::Renderer::RecompilationFlags::BRDFs);
			}
			if (ImGui::BeginChild("Preview", { 0,320 }, true, ImGuiWindowFlags_MenuBar))
			{
				drawTitle("Preview");
				ImGui::Text("Will show sphere or a\n simple scene.");

				ImGui::EndChild();
			}
		}
		void drawRightPanel(BRDFTextEditor::State& state, const GUIParams& gP)
		{
			float height = gP.mainWin.size.y;
			ImGui::InputTextMultiline("##Editor", state.textBuffer.data(), state.textBuffer.size(), ImVec2(0, 0.7f*height), ImGuiInputTextFlags_AllowTabInput);
			if (ImGui::BeginChild("Preview", { 0,0.3f*height }, true, ImGuiWindowFlags_MenuBar))
			{
				drawTitle("Compilation");
				ImGui::Text("Print console output here instead.");
				ImGui::Text("Will show compilation status and errors.");

				ImGui::EndChild();
			}
		}
	}
	BRDFTextEditor::BRDFTextEditor(const GUIParams & gP) :
		gP(gP)
	{
		state.textBuffer.resize(1 << 16);
	}

	void BRDFTextEditor::draw(Program & p)
	{
		auto leftPanel = [&matLib = p.getMatLib(), &s = this->state, &p = p](){drawLeftPanel(matLib, s, p); };
		auto rightPanel = [&s = this->state,&gP=gP](){ drawRightPanel(s, gP); };
		drawTwoPanels(gP, 0.2f, leftPanel, rightPanel);
	}
}