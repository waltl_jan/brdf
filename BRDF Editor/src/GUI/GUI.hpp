#ifndef BRDFEDITOR_GUI_GUI_HEADER
#define BRDFEDITOR_GUI_GUI_HEADER

#include <cstddef>
#include <memory>

#include "GUIParams.hpp"

namespace brdfEditor
{
	class Program;
}
namespace brdfEditor::gui
{
	class SceneRendering;
	class BRDFTextEditor;

	// Class that encapsulates all UI rendering.
	// Needs valid ImGui context
	class GUI
	{
	public:
		// --w,h: Width and height of the screen.
		GUI(float w, float h);
		~GUI();
		// Draws the GUI
		// --w,h: Current size of the screen.
		void draw(float w, float h, Program& p);
	private:
		enum class Window
		{
			renderedScene,
			//sceneEditing,
			brdfTextEditor,
			//brdfGraphs
		};

		void drawMenuBar(Program& p);

		GUIParams gParams;
		std::unique_ptr<SceneRendering> sceneRendering;
		std::unique_ptr<BRDFTextEditor> brdfTextEditor;

		Window selectedWindow;
	};
}

#endif