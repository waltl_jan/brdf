#ifndef BRDFEDITOR_GUI_BRDF_TEXT_EDITOR_HEADER
#define BRDFEDITOR_GUI_BRDF_TEXT_EDITOR_HEADER

#include <optional>

#include <src/OpenCLRenderer/Materials/MaterialsLibrary.hpp>

#include "GUIParams.hpp"

namespace brdfEditor
{
	class Program;
}
namespace brdfEditor::gui
{
	class BRDFTextEditor
	{
	public:
		using optSelected = std::optional<clRenderer::MaterialsLibrary::MatSourceHandle>;
		struct State
		{
			optSelected selected;
			std::vector<char> textBuffer;
		};
		// --gP: Captured, must remain valid for lifetime of this instance. 
		BRDFTextEditor(const GUIParams& gP);
		void draw(Program&p);
	private:
		State state;
		const GUIParams& gP;
	};
}

#endif