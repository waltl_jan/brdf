#ifndef BRDFEDITOR_GUI_GUIPARAMS_HEADER
#define BRDFEDITOR_GUI_GUIPARAMS_HEADER

#include <glm/vec2.hpp>
#include <ImGui/imgui.h>

namespace brdfEditor::gui
{
	// Various parameters of the GUI.
	// Should provide constants for other parts of the GUI 
	// to achieve consistent, resolution-free interface.
	struct GUIParams
	{
		// Screen resolution in pixels.
		glm::vec2 res;
		// Pixel coordinates of the main window of the program.
		struct
		{
			glm::vec2 size, off;
		}mainWin;
	};
}
#endif
